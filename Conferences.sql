-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: conferences
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(90) NOT NULL,
  `date` date NOT NULL,
  `location` varchar(45) NOT NULL,
  `time` time NOT NULL,
  `description` varchar(300) NOT NULL,
  `time_zone` varchar(45) DEFAULT NULL,
  `users_presence` int NOT NULL DEFAULT '0',
  `language_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_events_languages1_idx` (`language_id`),
  CONSTRAINT `fk_events_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (151,'Event 1','2020-01-10','Online','15:00:00','Description is here','UTC+01:00',0,0),(152,'Event 2','2020-01-10','Online','15:00:00','Description is here','UTC+01:00',0,0),(153,'Event 3','2020-02-10','Online','15:00:00','Description is here','UTC+01:00',0,0),(154,'Event 4','2020-03-10','Online','15:00:00','Description is here','UTC+01:00',0,0),(155,'Event 5','2020-03-10','Online','15:00:00','Description is here','UTC+01:00',0,0),(156,'Event 6','2020-04-10','Online','17:00:00','Description is here','UTC+01:00',0,0),(157,'Event 7','2020-05-10','Online','15:00:00','Description is here','UTC+01:00',0,0),(158,'Event 8','2020-10-20','Lonodn','15:00:00','Description is here',NULL,0,0),(159,'Event 9','2020-10-20','Berlin','15:00:00','Description is here',NULL,0,0),(160,'Event 10','2020-10-21','Paris','15:00:00','Description is here',NULL,0,0),(161,'Event 11','2020-10-23','Stokholm','16:00:00','Description is here',NULL,0,0),(162,'Event 12','2020-10-24','Ottawa','14:00:00','Description is here',NULL,0,0),(163,'Event 13','2020-10-24','Luxemburg','15:00:00','Description is here',NULL,0,0),(164,'Event 14','2020-10-25','Berlin','15:00:00','Description is here',NULL,0,0),(165,'Event 15','2020-09-26','Hamburg','16:00:00','Description is here',NULL,0,0),(166,'Event 16','2020-09-27','Boston','14:00:00','Description is here',NULL,0,0),(167,'Конференція 1','2020-05-11','Online','14:00:00','Опис конференції','UTC+02:00',9,1),(168,'Конференція 2','2020-05-08','Online','14:00:00','Опис конференції','UTC+02:00',9,1),(169,'Конференція 3','2020-05-12','Online','15:00:00','Опис конференції','UTC+02:00',9,1),(170,'Конференція 4','2020-05-21','Online','17:00:00','Опис конференції','UTC+02:00',9,1),(171,'Конференція 5','2020-05-25','Online','13:00:00','Опис конференції','UTC+02:00',9,1),(172,'Конференція 6','2020-05-23','Online','12:00:00','Опис конференції','UTC+02:00',9,1),(173,'Конференція 7','2020-05-22','Online','15:00:00','Опис конференції','UTC+02:00',9,1),(174,'Конференція 8','2020-05-08','Online','14:00:00','Опис конференції','UTC+02:00',9,1),(175,'Конференція 9','2020-10-08','Online','14:00:00','Опис конференції','UTC+02:00',9,1),(176,'Конференція 10','2020-11-08','Київ','14:00:00','Опис конференції','',5,1),(177,'Конференція 11','2020-10-11','Київ','14:00:00','Опис конференції','',5,1),(178,'Конференція 12','2020-10-12','Одеса','15:00:00','Опис конференції','',5,1),(179,'Конференція 13','2020-10-14','Вінниця','12:00:00','Опис конференції','',5,1),(180,'Конференція 14','2020-10-15','Харків','13:00:00','Опис конференції','',5,1),(181,'Конференція 15','2020-10-16','Львів','16:00:00','Опис конференції','',5,1),(182,'Конференція 16','2020-10-17','Київ','17:00:00','Опис конференції','',5,1),(183,'Событие 1','2020-05-11','Online','14:00:00','Описание конференции','UTC+02:00',9,2),(184,'Событие 2','2020-05-08','Online','14:00:00','Описание конференции','UTC+02:00',9,2),(185,'Событие 3','2020-05-12','Online','15:00:00','Описание конференции','UTC+02:00',9,2),(186,'Событие 4','2020-05-21','Online','17:00:00','Описание конференции','UTC+02:00',9,2),(187,'Событие 5','2020-05-25','Online','13:00:00','Описание конференции','UTC+02:00',9,2),(188,'Событие 6','2020-05-23','Online','12:00:00','Описание конференции','UTC+02:00',9,2),(189,'Событие 7','2020-05-22','Online','15:00:00','Описание конференции','UTC+02:00',9,2),(190,'Событие 8','2020-05-08','Online','14:00:00','Описание конференции','UTC+02:00',9,2),(191,'Событие 9','2020-10-08','Online','14:00:00','Описание конференции','UTC+02:00',9,2),(192,'Событие 10','2020-11-08','Киев','14:00:00','Описание конференции','',5,2),(193,'Событие 11','2020-10-11','Киев','14:00:00','Описание конференции','',5,2),(194,'Событие 12','2020-10-12','Одесса','15:00:00','Описание конференции','',5,2),(195,'Событие 13','2020-10-14','Виниица','12:00:00','Описание конференции','',5,2),(196,'Событие 14','2020-10-15','Харьков','13:00:00','Описание конференции','',5,2),(197,'Событие 15','2020-10-16','Львов','16:00:00','Описание конференции','',5,2),(198,'Событие 16','2020-10-17','Киев','17:00:00','Описание конференции','',5,2);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (0,'English'),(2,'Russian'),(1,'Ukrainian');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reports` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `event_id` int NOT NULL,
  `theme_id` int NOT NULL,
  `status_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_report_event1_idx` (`event_id`),
  KEY `fk_reports_themes1_idx` (`theme_id`),
  KEY `fk_reports_statuses1_idx` (`status_id`),
  CONSTRAINT `fk_report_event1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_reports_statuses1` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`),
  CONSTRAINT `fk_reports_themes1` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` VALUES (71,'Report 1',158,2,0),(72,'Report 2',158,5,0),(73,'Report 3',158,6,0),(74,'Report 1',160,4,0),(75,'Доповідь 1',176,5,0),(76,'Доклад 1',192,3,0);
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (0,'Moderator'),(1,'Speaker'),(2,'User');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statuses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES (0,'Approved'),(3,'Submitted by user'),(1,'Suggested by moderator'),(2,'Suggested by user');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `themes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `name_en` varchar(45) NOT NULL,
  `name_ua` varchar(45) NOT NULL,
  `name_ru` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (1,'Science','Science','Наука','Наука'),(2,'Space','Space','Космос','Космос'),(3,'Nuclear power','Nuclear power','Ядерна енергетика','Ядерная енерегтика'),(4,'Sport','Sport','Спорт','Спорт'),(5,'Culture','Culture','Культура','Клуьтура'),(6,'Industry','Industry','Індустрія','Индустрия'),(7,'Pedagogy','Pedagogy','Педагогіка','Педагогика ');
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role_id` int NOT NULL,
  `language_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`name`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_roles_idx` (`role_id`),
  KEY `fk_users_languages1_idx` (`language_id`),
  CONSTRAINT `fk_users_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  CONSTRAINT `fk_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'SuperAdmin','admin@gmail.com','1',0,0),(2,'Random speaker','speaker@gmail.com','1',1,1),(3,'Random speaker2','speaker2@gmail.com','1',1,1),(4,'Avarange user','user3@gmail.com','1',2,1),(5,'Avarange user2','user2@gmail.com','1',2,2),(15,'Токарчук Дмитро','user@gmail.com','1',2,2),(17,'Blabla Blabla','dima014ty@gmail.com','1',2,0),(22,'Thomas Edison','speaker3@gmail.com','1',1,0),(24,'Tokarchu Dilsdnmfo','dima013ty@gmail.com','qwerty7U',2,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_events`
--

DROP TABLE IF EXISTS `users_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_events` (
  `user_id` int NOT NULL,
  `event_id` int NOT NULL,
  `has_visited` tinyint DEFAULT '0',
  PRIMARY KEY (`user_id`,`event_id`),
  KEY `fk_users_has_event_event1_idx` (`event_id`),
  KEY `fk_users_has_event_users1_idx` (`user_id`),
  CONSTRAINT `fk_users_has_event_event1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_has_event_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_events`
--

LOCK TABLES `users_events` WRITE;
/*!40000 ALTER TABLE `users_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_reports`
--

DROP TABLE IF EXISTS `users_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_reports` (
  `user_id` int NOT NULL,
  `report_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`report_id`),
  KEY `fk_users_has_conferences_conferences1_idx` (`report_id`),
  KEY `fk_users_has_conferences_users1_idx` (`user_id`),
  CONSTRAINT `fk_users_has_conferences_conferences1` FOREIGN KEY (`report_id`) REFERENCES `reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_has_conferences_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_reports`
--

LOCK TABLES `users_reports` WRITE;
/*!40000 ALTER TABLE `users_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_reports` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-16 17:42:43
