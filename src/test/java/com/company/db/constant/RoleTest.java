package com.company.db.constant;

import com.company.db.entity.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class RoleTest {


    @Test
    public void shouldGetRole(){
        User user = new User();
        user.setRoleId(0);
        Role expectedRole = Role.MODERATOR;
        Role actualRole = Role.getRole(user);


        Assert.assertEquals(expectedRole,actualRole);
    }

    @Test
    public void shouldGetName(){
        Role role = Role.MODERATOR;
        String expectedName = "moderator";
        String actualName = role.getName();

        Assert.assertEquals(expectedName,actualName);
    }

    @Test
    public void shouldGetId(){
        Role role = Role.SPEAKER;
        int expectedId = 1;
        int actualId = role.getId();

        Assert.assertEquals(expectedId,actualId);
    }

}
