package com.company.db.constant;

import com.company.db.entity.Report;
import com.company.db.entity.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class StatusTest {

    @Test
    public void shouldGetRole(){
        Status expectedStatus = Status.APPROVED;
        Status actualStatus;

        Report report = new Report();
        report.setStatusId(0);

        actualStatus = Status.getRole(report);

        Assert.assertEquals(expectedStatus,actualStatus);


    }

    @Test
    public void shouldGetStatusId(){
        Status status = Status.SUGGESTED_BY_MODERATOR;
        int expectedId = 1;
        int actualId = status.getId();

        Assert.assertEquals(expectedId,actualId);
    }

    @Test
    public void shouldGetName(){
        Status status = Status.APPROVED;
        String expectedName = "approved";
        String actualName = status.getName();

        Assert.assertEquals(expectedName,actualName);


    }


}
