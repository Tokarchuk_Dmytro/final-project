package com.company.db.constant;

import com.company.db.DBUtil;
import com.company.db.entity.Event;
import com.company.db.entity.User;
import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import javax.naming.*;

import java.sql.Connection;
import java.sql.SQLException;


public class LanguageTest {


    @Test
    public void shouldGetLanguage(){
        User user = new User();
        user.setLanguageId(1);
        Language expectedLanguage = Language.UKRAINIAN;
        Language actualLanguage = Language.getLanguage(user);

        Assert.assertEquals(expectedLanguage,actualLanguage);
    }

    @Test
    public void shouldGetId(){
        Language language = Language.RUSSIAN;
        int expectedId = 2;
        int actualId = language.getId();

        Assert.assertEquals(expectedId,actualId);
    }

    @Test
    public void shouldGetName(){
        Language language = Language.ENGLISH;
        String expectedName = "English";
        String actualName = language.getName();

        Assert.assertEquals(expectedName,actualName);
    }

    @Test
    public void shouldGetIsoCode(){
        Language language = Language.UKRAINIAN;
        String expectedIsoCode = "uk";
        String actualIsoCode = language.getIsoCode();

        Assert.assertEquals(expectedIsoCode,actualIsoCode);
    }
}
