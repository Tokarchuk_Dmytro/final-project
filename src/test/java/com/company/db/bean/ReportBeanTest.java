package com.company.db.bean;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class ReportBeanTest {

    @Test
    public void shouldSetValues(){
        ReportBean reportBean = new ReportBean();

        reportBean.setReportId(1);
        reportBean.setUserId(1);
        reportBean.setThemeId(1);
        reportBean.setStatusId(1);
        reportBean.setEventId(1);
        reportBean.setReportName("name r");
        reportBean.setUserName("name u");
        reportBean.setThemeName("name t");
        reportBean.setStatusName("name s");
        reportBean.setEventName("name e");


        Assert.assertEquals(1,reportBean.getReportId());
        Assert.assertEquals(1,reportBean.getUserId());
        Assert.assertEquals(1,reportBean.getThemeId());
        Assert.assertEquals(1,reportBean.getStatusId());
        Assert.assertEquals(1,reportBean.getEventId());
        Assert.assertEquals("name r",reportBean.getReportName());
        Assert.assertEquals("name u",reportBean.getUserName());
        Assert.assertEquals("name t",reportBean.getThemeName());
        Assert.assertEquals("name s",reportBean.getStatusName());
        Assert.assertEquals("name e",reportBean.getEventName());
    }
}
