package com.company.db.bean;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class EventBeanTest {

    @Test
    public void shouldSetValues(){
        EventBean eventBean = new EventBean();
        Date date = new Date();
        eventBean.setId(1);
        eventBean.setName("name");
        eventBean.setDate(date);
        eventBean.setLocation("loc");
        eventBean.setDescription("desc");
        eventBean.setTime("00:00");
        eventBean.setTimeZone("UTC+02:00");
        eventBean.setUsersPresence(1);
        eventBean.setLanguageId(1);
        eventBean.setSubscribers(1);
        eventBean.setReports(1);


        Assert.assertEquals(1,eventBean.getId());
        Assert.assertEquals("name",eventBean.getName());
        Assert.assertEquals(date,eventBean.getDate());
        Assert.assertEquals("loc",eventBean.getLocation());
        Assert.assertEquals("desc",eventBean.getDescription());
        Assert.assertEquals("00:00",eventBean.getTime());
        Assert.assertEquals("UTC+02:00",eventBean.getTimeZone());
        Assert.assertEquals(1,eventBean.getUsersPresence());
        Assert.assertEquals(1,eventBean.getLanguageId());
        Assert.assertEquals(1,eventBean.getSubscribers());
        Assert.assertEquals(1,eventBean.getReports());
    }
}
