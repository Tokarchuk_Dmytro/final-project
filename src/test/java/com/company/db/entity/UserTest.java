package com.company.db.entity;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import javax.jws.soap.SOAPBinding;

public class UserTest {

    @Test
    public void shouldSetValues(){
        User user = new User();

        user.setId(1);
        user.setName("name");
        user.setEmail("email");
        user.setPassword("password");
        user.setRoleId(1);
        user.setLanguageId(1);


        Assert.assertEquals(1,user.getId());
        Assert.assertEquals("name",user.getName());
        Assert.assertEquals("email",user.getEmail());
        Assert.assertEquals("password",user.getPassword());
        Assert.assertEquals(1,user.getRoleId());
        Assert.assertEquals(1,user.getLanguageId());
    }
}
