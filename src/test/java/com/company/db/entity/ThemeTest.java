package com.company.db.entity;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class ThemeTest {

    @Test
    public void shouldSetValues(){
        Theme theme = new Theme();

        theme.setId(1);
        theme.setName("name");

        Assert.assertEquals(1,theme.getId());
        Assert.assertEquals("name",theme.getName());
    }


    @Test
    public void shouldCompareObjets(){
        Theme theme1 = new Theme();
        theme1.setId(1);
        Theme theme2 = new Theme();
        theme2.setId(1);

        Assert.assertEquals(theme1, theme2);
    }

    @Test
    public void shouldGenerateHashCode(){

    }
}
