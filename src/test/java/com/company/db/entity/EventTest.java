package com.company.db.entity;


import org.junit.*;

import java.util.Date;

public class EventTest {


    @Test
    public void shouldSetValues(){
        Event event = new Event();

        event.setId(1);
        event.setName("Name");
        Date date = new Date();
        event.setDate(date);
        event.setLocation("Online");
        event.setTime("10:00");
        event.setTimeZone("UTC+02:00");
        event.setUsersPresence(0);
        event.setLanguageId(0);

        Assert.assertEquals(1,event.getId());
        Assert.assertEquals("Name",event.getName());
        Assert.assertEquals(date,event.getDate());
        Assert.assertEquals("Online",event.getLocation());
        Assert.assertEquals("10:00",event.getTime());
        Assert.assertEquals("UTC+02:00",event.getTimeZone());
        Assert.assertEquals(0,event.getUsersPresence());
        Assert.assertEquals(0,event.getLanguageId());
    }
}
//    @Before
//    public void setUpStreams() {
//        System.setOut(new PrintStream(outContent));
//    }
//    @After
//    public void restoreStreams() {
//        System.setOut(originalOut);
//    }
//
//    @AfterClass
//    public static void deleteFiles() throws IOException {
//        Files.deleteIfExists(Paths.get("output.dom.xml"));
//        Files.deleteIfExists(Paths.get("output.sax.xml"));
//        Files.deleteIfExists(Paths.get("output.stax.xml"));
//    }
//
//    @Test
//    public  void mainTest() throws Exception {
//        Demo.main(null);
//
//        Assert.assertNotNull(outContent);
//
//    }