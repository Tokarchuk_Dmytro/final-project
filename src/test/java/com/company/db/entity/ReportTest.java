package com.company.db.entity;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class ReportTest {

    @Test
    public void shouldSetValues(){
        Report report = new Report();
        report.setId(1);
        report.setName("name");
        report.setThemeId(1);
        report.setEventId(1);
        report.setStatusId(1);

        Assert.assertEquals(1,report.getId());
        Assert.assertEquals("name",report.getName());
        Assert.assertEquals(1,report.getThemeId());
        Assert.assertEquals(1,report.getEventId());
        Assert.assertEquals(1,report.getStatusId());
    }
}
