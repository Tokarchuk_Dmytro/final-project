package com.company.db.entity;

import java.util.Date;
import java.util.Set;

/**
 * Event entity
 */

public class Event {

    private long id;

    private String name;

    private Date date;

    private String location;

    private String description;

    private String time;

    private String timeZone;

    private int usersPresence;

    private int languageId;



    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUsersPresence() {
        return usersPresence;
    }

    public void setUsersPresence(int usersPresence) {
        this.usersPresence = usersPresence;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }
}
