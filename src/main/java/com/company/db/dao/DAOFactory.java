package com.company.db.dao;


import com.company.db.dao.mysql.MySqlDAOFactory;

/**
 *  Class for DAO pattern realization
 */
public abstract class DAOFactory {

    public static final String MYSQL = "MySQL";


    public static DAOFactory getDAOFactory(){
        return MySqlDAOFactory.getInstance();
    }

    public static DAOFactory getDAOFactory(String name) {
        if (MYSQL.equalsIgnoreCase(name)) {
            return MySqlDAOFactory.getInstance();
        }
        throw new RuntimeException("Unknown factory");
    }

    public abstract UserDAO getUserDAO();

    public abstract ReportDAO getReportDAO();

    public abstract EventDAO getEventDAO();

    public abstract ThemeDAO getThemeDAO();

}
