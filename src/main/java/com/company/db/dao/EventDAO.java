package com.company.db.dao;

import com.company.db.bean.EventBean;
import com.company.db.entity.Event;

import java.sql.SQLException;
import java.util.List;

/**
 *  Interface for realizing event DAO
 */
public interface EventDAO {

    void insert(Event event) throws SQLException;

    void update(Event event) throws SQLException;

    void delete(Event event) throws SQLException;

    Event getEventById(long id) throws SQLException;

    List<Event> getEventsByUserId(long userId) throws SQLException;

    int getEventsAmount(String eventRelevance,List<Integer> languages) throws SQLException;

    List<Event> getEventsByNameLike(String name) throws SQLException;

    List<Event> getSpeakerEvents(long userId) throws SQLException;

    List<EventBean> getEventBeansFromTo(int from , int range, String sortBy, String orderBy, String relevance, List<Integer> languages) throws SQLException;

    void incrementUsersPresence(long eventId) throws SQLException;

    void confirmUserPresence(long userId, long eventId) throws SQLException;
}
