package com.company.db.dao;


import com.company.db.constant.Language;
import com.company.db.constant.Status;
import com.company.db.bean.ReportBean;
import com.company.db.entity.Report;

import java.sql.SQLException;
import java.util.List;

/**
 *  Interface for realizing report DAO
 */
public interface ReportDAO {

    void insert(Report report) throws SQLException;

    void update(Report report) throws SQLException;

    void delete(Report report) throws SQLException;

    List<ReportBean> getReportBeansByEventIdWithStatus(long eventId, Language locale, Status... statuses) throws SQLException;

    List<ReportBean> getReportBeansByUserIdWithStatus(long userId, Status ... statuses) throws SQLException;

    List<ReportBean> getReportBeansWithStatus(Status ... statuses) throws SQLException;

    ReportBean getReportBeanById(long id) throws SQLException;

    void setUserForReport(long reportId, long userId) throws SQLException;

    void updateStatus(long id,Status status) throws SQLException;

    void deleteUserFromReport(long userId, long reportId) throws SQLException;

    void deleteUsersFromReportExcept(long exceptionId, long reportId) throws SQLException;

    int countReportsByStatus(Status ... status) throws SQLException;

    int countSuggestedReportsByUserId(long userId) throws SQLException;


}
