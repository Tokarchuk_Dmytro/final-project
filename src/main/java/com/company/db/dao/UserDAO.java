package com.company.db.dao;


import com.company.db.constant.Role;
import com.company.db.entity.User;

import java.sql.SQLException;
import java.util.List;

/**
 *  Interface for realizing user DAO
 */
public interface UserDAO {

    void insert(User user) throws SQLException;

    void update(User user) throws SQLException;

    void delete(User user) throws SQLException;

    boolean isUserEmailExist(String email) throws SQLException;

    User findUserByEmail(String login) throws SQLException;

    void registerUserForEvent(long userId,long eventId) throws SQLException;

    void removeUserFromEvent(long userId,long eventId) throws SQLException;

    List<User> getUsersByRole(Role role) throws SQLException;



}
