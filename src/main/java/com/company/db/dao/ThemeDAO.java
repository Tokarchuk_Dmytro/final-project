package com.company.db.dao;

import com.company.db.constant.Language;
import com.company.db.entity.Theme;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 *  Interface for realizing theme DAO
 */
public interface ThemeDAO {

    void insert(Map<Language,String> names) throws SQLException;

    List<Theme> getAllThemes(Language locale) throws SQLException;


}
