package com.company.db.dao.mysql;


import com.company.db.DBUtil;
import com.company.db.EntityMapper;
import com.company.db.Fields;
import com.company.db.constant.Role;
import com.company.db.dao.UserDAO;
import com.company.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlUserDAO implements UserDAO {

    private static MySqlUserDAO instance;

    public static synchronized MySqlUserDAO getInstance() {
        if (instance == null) {
            instance = new MySqlUserDAO();
        }
        return instance;
    }

    private MySqlUserDAO() {
    }


    /**
     * Inserts new user in DB
     *
     * @param user User entity to be inserted
     * @throws SQLException When query fails
     */
    @Override
    public void insert(User user) throws SQLException {
        String query = "INSERT INTO users(name,email,password,role_id,language_id) values(?,?,?,?,?);";
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;

        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, user.getName());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setInt(4, user.getRoleId());
            statement.setInt(5, user.getLanguageId());

            if (statement.executeUpdate() > 0) {
                rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getLong(1));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
    }

    /**
     * Updates user in DB. Requires {@link User#id}
     *
     * @param user User to be inserted.
     * @throws SQLException When query fails
     */
    @Override
    public void update(User user) throws SQLException {
        String query = "UPDATE users SET name = ?, email = ?, password = ?, role_id = ?, language_id = ? WHERE (id = ?);";
        Connection con = null;
        PreparedStatement statement;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setString(1, user.getName());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setInt(4, user.getRoleId());
            statement.setInt(5, user.getLanguageId());
            statement.setLong(6, user.getId());
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        } finally {
            con.close();
        }
    }

    @Override
    public void delete(User user) throws SQLException {
        throw new UnsupportedOperationException();
    }

    /**
     * Checks if user with specified email exists in already exists
     *
     * @param email Sting to be checked
     * @return If exists true, otherwise false
     * @throws SQLException When query fails
     */
    @Override
    public boolean isUserEmailExist(String email) throws SQLException {
        String query = "SELECT * FROM users WHERE email =?;";
        Connection con = null;
        PreparedStatement statement;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setString(1, email);

            if (statement.executeQuery().next()) {
                return true;
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            con.close();
        }
        return false;
    }

    /**
     * Gets user by his email
     *
     * @param email User email to be found
     * @return Found user
     * @throws SQLException When query fails
     */
    @Override
    public User findUserByEmail(String email) throws SQLException {
        String query = "SELECT * FROM users WHERE email=?;";
        User user = new User();
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setString(1, email);
            rs = statement.executeQuery();
            if (rs.next()) {
                user.setId(rs.getLong(1));
                user.setName(rs.getString(2));
                user.setEmail(rs.getString(3));
                user.setPassword(rs.getString(4));
                user.setRoleId(rs.getInt(5));
                user.setLanguageId(rs.getInt(6));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return user;
    }


    /**
     * Establishes a user-event relation by theirs ids.
     * @param userId User id
     * @param eventId Event id
     * @throws SQLException When query fails.
     */
    @Override
    public void registerUserForEvent(long userId, long eventId) throws SQLException {
        String query = "INSERT INTO users_events (user_id,event_id) VALUES (?, ?);";
        Connection con = null;
        PreparedStatement stm;
        try {
            con = DBUtil.getConnection();
            stm = con.prepareStatement(query);
            stm.setLong(1, userId);
            stm.setLong(2, eventId);
            stm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            con.close();
        }
    }


    /**
     * Removes user-event relation by their ids
     * @param userId User id
     * @param eventId Event id
     * @throws SQLException When query fails
     */
    @Override
    public void removeUserFromEvent(long userId, long eventId) throws SQLException {
        String query = "DELETE FROM users_events WHERE (user_id = ?) and (event_id = ?);";
        Connection con = null;
        PreparedStatement statement;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1, userId);
            statement.setLong(2, eventId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        } finally {
            con.close();
        }
    }


    /**
     * Gets all users whith spesified role
     * @see Role
     * @param role Role object
     * @return List of found users
     * @throws SQLException When query fails
     */
    @Override
    public List<User> getUsersByRole(Role role) throws SQLException {
        String query = "Select * from users where role_id = ?;";
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        List<User> users = new ArrayList<>();
        EntityMapper<User> mapper = new UserMapper();
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1, role.getId());
            rs = statement.executeQuery();
            while (rs.next()) {
                users.add(mapper.mapRow(rs));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        } finally {
            con.close();
        }
        return users;
    }
    /**
     * Class allows to extract fields from result set
     * into user object
     *
     */
    private static class UserMapper implements EntityMapper<User> {

        @Override
        public User mapRow(ResultSet rs) {
            try {
                User user = new User();
                user.setId(rs.getLong(Fields.ID));
                user.setName(rs.getString(Fields.USER_NAME));
                user.setEmail(rs.getString(Fields.USER_EMAIL));
                user.setPassword(rs.getString(Fields.USER_PASSWORD));
                user.setRoleId(rs.getInt(Fields.USER_ROLE_ID));
                user.setLanguageId(rs.getInt(Fields.USER_LANGUAGE_ID));
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}
