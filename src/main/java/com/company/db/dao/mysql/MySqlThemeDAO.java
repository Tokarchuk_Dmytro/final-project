package com.company.db.dao.mysql;

import com.company.db.DBUtil;
import com.company.db.EntityMapper;
import com.company.db.Fields;
import com.company.db.constant.Language;
import com.company.db.dao.ThemeDAO;
import com.company.db.entity.Theme;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MySqlThemeDAO implements ThemeDAO {

    private static MySqlThemeDAO instance;

    public static synchronized MySqlThemeDAO getInstance(){
        if(instance == null){
            instance = new MySqlThemeDAO();
        }
        return instance;
    }

    private MySqlThemeDAO(){}


    /**
     * Inserts new theme into DB
     * @param names Map of theme names on different languages
     * @throws SQLException
     */
    @Override
    public void insert(Map<Language,String> names) throws SQLException {
        String query = "INSERT INTO themes ( name_en, name_ua, name_ru) VALUES (?, ?, ?);";
        Connection con = null;
        PreparedStatement statement;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setString(1,names.get(Language.ENGLISH));
            statement.setString(2,names.get(Language.UKRAINIAN));
            statement.setString(3,names.get(Language.RUSSIAN));
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }finally {
            con.close();
        }
    }

    /**
     * Retrieves all themes with specified locale
     * @param locale Locale which specifies theme name language
     * @return List of themes entities
     * @throws SQLException
     */
    @Override
    public List<Theme> getAllThemes(Language locale) throws SQLException {
        String query = "select themes.id, themes.name_& as name from themes;";
        Connection con = null;
        Statement statement;
        ResultSet rs;
        List<Theme> themes = new ArrayList<>();
        try {
            con = DBUtil.getConnection();
            statement = con.createStatement();
            query = query.replace("&",locale.getIsoCode());
            rs = statement.executeQuery(query);
            ThemeMapper mapper = new ThemeMapper();
            while (rs.next()){
                themes.add(mapper.mapRow(rs));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }finally {
            con.close();
        }
        return themes;
    }

    /**
     * Class allows to extract fields from result set
     * into theme object
     *
     */
    private static class ThemeMapper implements EntityMapper<Theme> {

        @Override
        public Theme mapRow(ResultSet rs) {
            try {
                Theme theme = new Theme();
                theme.setId(rs.getLong(Fields.ID));
                theme.setName(rs.getString(Fields.THEME_NAME));
                return theme;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
