package com.company.db.dao.mysql;

import com.company.db.*;
import com.company.db.bean.ReportBean;
import com.company.db.constant.Language;
import com.company.db.constant.Status;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.Report;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
/**
 * DAO for accessing event entities
 */
public class MySqlReportDAO implements ReportDAO {

    private static MySqlReportDAO instance;

    public static synchronized MySqlReportDAO getInstance() {
        if (instance == null) {
            instance = new MySqlReportDAO();
        }
        return instance;
    }

    private MySqlReportDAO() {
    }

    /**
     * Inserts a report in DB
     * @param report Report to be inserted
     * @throws SQLException When query fails
     */
    @Override
    public void insert(Report report) throws SQLException {
        String query = "INSERT INTO reports (name, event_id, theme_id, status_id) VALUES (?, ?, ?, ?);";
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, report.getName());
            statement.setLong(2, report.getEventId());
            statement.setLong(3, report.getThemeId());
            statement.setLong(4, report.getStatusId());
            if (statement.executeUpdate() > 0) {
                rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    report.setId(rs.getLong(1));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        } finally {
            con.close();
        }
    }

    /**
     * Updated a report in DB
     * @param report Report to be updated
     * @throws SQLException When query fails
     */
    @Override
    public void update(Report report) throws SQLException {
        String query = "UPDATE reports SET " +
                "name = ?, theme_id = ? WHERE (id = ?);";
        Connection con = null;
        PreparedStatement statement = null;

        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setString(1, report.getName());
            statement.setLong(2, report.getThemeId());
            statement.setLong(3, report.getId());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }

    }

    /**
     * Delets a report in DB
     * @param report Report to be deleted
     * @throws SQLException
     */
    @Override
    public void delete(Report report) throws SQLException {
        String query = "delete from reports where id =?;";
        Connection con = null;
        PreparedStatement statement = null;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1,report.getId());
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            con.close();
        }
    }

    /**
     * Gets reports related to the event by event id with specified parameters
     *
     * @param eventId Event id
     * @param locale Result entities locale
     * @param status Array of status identifiers
     * @return List of report beans which satisfies parameters
     * @throws SQLException When query fails
     */
    @Override
    public List<ReportBean> getReportBeansByEventIdWithStatus(long eventId, Language locale, Status... status) throws SQLException {
        List<ReportBean> reportBeans = new ArrayList<>();
        String query = "select reports.name as report_name, reports.id as report_id, " +
                "themes.name_& as theme_name , themes.id as theme_id,  " +
                "users.name as user_name, users.id as user_id, " +
                "statuses.name as status_name, statuses.id as status_id, "+
                "events.id as event_id, events.name as event_name\n"+
                "from reports\n" +
                "join events on events.id = reports.event_id " +
                "join statuses on statuses.id = reports.status_id "+
                "left join themes on reports.theme_id = themes.id " +
                "left join users_reports on reports.id = users_reports.report_id " +
                "left join users on users.id = users_reports.user_id " +
                "where reports.event_id = ? and reports.status_id in ";
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        query = query.replace("&",locale.getIsoCode());
        String statuses = Arrays.stream(status)
                .map(x -> Integer.toString(x.getId()))
                .collect(Collectors.joining(",", "(", ")")) + ";";
        query += statuses;

        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1, eventId);
            rs = statement.executeQuery();
            ReportBeanMapper mapper = new ReportBeanMapper();

            while (rs.next()) {
                reportBeans.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        return reportBeans;
    }


    /**
     * Gets reports related to the user by user id with specified parameters
     *
     * @param userId User id
     * @param status Array of status identifiers
     * @return List of report beans which satisfies parameters
     * @throws SQLException When query fails
     */
    @Override
    public List<ReportBean> getReportBeansByUserIdWithStatus(long userId, Status... status) throws SQLException {
        List<ReportBean> reportBeans = new ArrayList<>();
        String query = "select reports.name as report_name, reports.id as report_id,\n" +
                "themes.name as theme_name, themes.id as theme_id,\n" +
                "users.name as user_name, users.id as user_id,\n" +
                "statuses.name as status_name, statuses.id as status_id,\n" +
                "events.id as event_id, events.name as event_name\n"+
                "from reports\n" +
                "join events on events.id = reports.event_id " +
                "join statuses on statuses.id = reports.status_id\n" +
                "join themes on reports.theme_id = themes.id\n" +
                "join users_reports on users_reports.report_id = reports.id\n" +
                "join users on users.id = users_reports.user_id\n" +
                "where users.id = ? and status_id in & "+
                "order by status_id;";

        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        String statuses = Arrays.stream(status)
                .map(x -> Integer.toString(x.getId()))
                .collect(Collectors.joining(",", "(", ")"));
        query = query.replace("&",statuses);
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1, userId);
            rs = statement.executeQuery();
            ReportBeanMapper mapper = new ReportBeanMapper();

            while (rs.next()) {
                reportBeans.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        return reportBeans;
    }


    /**
     * Gets report beans with specified parameters
     * @param statuses Array of report statuses
     * @return List of report beans which satisfies parameters
     * @throws SQLException When query fails
     */
    @Override
    public List<ReportBean> getReportBeansWithStatus(Status... statuses) throws SQLException {
        List<ReportBean> reportBeans = new ArrayList<>();
        String query = "select reports.name as report_name, reports.id as report_id,\n" +
                "themes.name as theme_name, themes.id as theme_id,\n" +
                "users.name as user_name, users.id as user_id,\n" +
                "statuses.name as status_name, statuses.id as status_id,\n" +
                "events.id as event_id, events.name as event_name\n"+
                "from reports\n" +
                "join events on events.id = reports.event_id " +
                "join statuses on statuses.id = reports.status_id\n" +
                "join themes on reports.theme_id = themes.id\n" +
                "join users_reports on users_reports.report_id = reports.id\n" +
                "join users on users.id = users_reports.user_id\n" +
                "where status_id in & ;";

        Connection con = null;
        Statement statement;
        ResultSet rs;
        String statusSting= Arrays.stream(statuses)
                .map(x -> Integer.toString(x.getId()))
                .collect(Collectors.joining(",", "(", ")"));
        query = query.replace("&",statusSting);
        try {
            con = DBUtil.getConnection();
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            ReportBeanMapper mapper = new ReportBeanMapper();

            while (rs.next()) {
                reportBeans.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        return reportBeans;
    }


    /**
     * Gets report related to the user by user id
     * @param id User id
     * @return A report entity
     * @throws SQLException When query fails
     */
    @Override
    public ReportBean getReportBeanById(long id) throws SQLException {
        String query = "select reports.name as report_name, reports.id as report_id,\n" +
                "themes.name as theme_name, themes.id as theme_id,\n" +
                "users.name as user_name, users.id as user_id,\n" +
                "statuses.name as status_name, statuses.id as status_id,\n" +
                "events.id as event_id, events.name as event_name\n"+
                "from reports\n" +
                "join events on events.id = reports.event_id " +
                "join statuses on statuses.id = reports.status_id\n" +
                "join themes on reports.theme_id = themes.id\n" +
                "left join users_reports on users_reports.report_id = reports.id\n" +
                "left join users on users.id = users_reports.user_id\n" +
                "where reports.id = ?;";
        Connection con = null;
        PreparedStatement statement = null;
        ReportBean reportBean = null;
        ResultSet rs;
        ReportBeanMapper mapper = new ReportBeanMapper();
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1,id);
            rs = statement.executeQuery();
            if(rs.next()) {
                reportBean = mapper.mapRow(rs);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        } finally {
            con.close();
        }
        return reportBean;
    }

    /**
     * Sets relation with user and report in DB
     * @param reportId Report id to relate to
     * @param userId User id to relate to
     * @throws SQLException When query fails
     */
    @Override
    public void setUserForReport(long reportId, long userId) throws SQLException {
        String query = "INSERT INTO users_reports (user_id, report_id) VALUES (?, ?);";
        Connection con = null;
        PreparedStatement statement;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1, userId);
            statement.setLong(2, reportId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            con.close();
        }
    }

    /**
     * Updates report status in DB
     * @see Status
     * @param id Report id which will be updated
     * @param status New report status
     * @throws SQLException When query fails
     */
    @Override
    public void updateStatus(long id, Status status) throws SQLException {
        String query = "UPDATE reports SET " +
                "status_id = ? WHERE (id = ?);";
        Connection con = null;
        PreparedStatement statement = null;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setInt(1, status.getId());
            statement.setLong(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
    }


    /**
     * Deletes user-report relation
     * @param userId user id to be changed
     * @param reportId report to be changed
     * @throws SQLException When query fails
     */
    @Override
    public void deleteUserFromReport(long userId, long reportId) throws SQLException {
        String query ="delete from users_reports where user_id = ? and report_id = ?;";
        Connection con = null;
        PreparedStatement statement;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1,userId);
            statement.setLong(2,reportId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }finally {
            con.close();
        }
    }

    /**
     * Deletes all user-report relation with one exception
     * @param exceptionId User exception id
     * @param reportId Report id which user relation needs to be deleted
     * @throws SQLException When query fails
     */
    @Override
    public void deleteUsersFromReportExcept(long exceptionId, long reportId) throws SQLException {
        String query = "delete from users_reports where user_id != ? and report_id =?;;";
        Connection con = null;
        PreparedStatement statement;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1,exceptionId);
            statement.setLong(2,reportId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw  throwables;
        }finally {
            con.close();
        }
    }

    /**
     * Counts reports number with specified status
     * @see Status
     * @param status Array of status ids
     * @return Number of reports number with specified status
     * @throws SQLException When query fails
     */
    @Override
    public int countReportsByStatus(Status ... status) throws SQLException {
        String query = "SELECT count(reports.id) from reports\n" +
                "where status_id in ?";
        Connection con = null;
        Statement statement;
        ResultSet rs;
        int count=0;
        String statusSting = Arrays.stream(status)
                .map(x -> Integer.toString(x.getId()))
                .collect(Collectors.joining(",", "(", ")"))+";";
        query = query.replace("?",statusSting);
        try {
            con = DBUtil.getConnection();
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            if(rs.next()){
                count = rs.getInt(1);
            }
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }finally {
            con.close();
        }
    }

    /**
     * Counts related to user reports with status 'suggested by user' by user id
     * @param userId User id
     * @return Number of reports
     * @throws SQLException
     */
    @Override
    public int countSuggestedReportsByUserId(long userId) throws SQLException {
        String query = "select count(r.id) from reports as r\n" +
                "join users_reports as ur on ur.report_id = r.id\n" +
                "join users as u on u.id = ur.user_id\n" +
                "where u.id = ? and r.status_id = 1;";
        Connection con = null;
        PreparedStatement statement;
        ResultSet resultSet;
        int count = 0;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1,userId);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                count =  resultSet.getInt(1);
            }
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }finally {
            con.close();
        }
    }

    /**
     * Class allows to extract fields from result set
     * into report object
     *
     */
    private static class ReportBeanMapper implements EntityMapper<ReportBean> {

        @Override
        public ReportBean mapRow(ResultSet rs) {
            try {
                ReportBean bean = new ReportBean();
                bean.setReportId(rs.getLong(Fields.REPORT_BEAN_REPORT_ID));
                bean.setThemeId(rs.getLong(Fields.REPORT_BEAN_THEME_ID));
                bean.setUserId(rs.getLong(Fields.REPORT_BEAN_USER_ID));
                bean.setStatusId(rs.getInt(Fields.REPORT_BEAN_STATUS_ID));
                bean.setEventId(rs.getInt(Fields.REPORT_BEAN_EVENT_ID));
                bean.setReportName(rs.getString(Fields.REPORT_BEAN_REPORT_NAME));
                bean.setThemeName(rs.getString(Fields.REPORT_BEAN_THEME_NAME));
                bean.setUserName(rs.getString(Fields.REPORT_BEAN_USER_NAME));
                bean.setStatusName(rs.getString(Fields.REPORT_BEAN_STATUS_NAME));
                bean.setEventName(rs.getString(Fields.REPORT_BEAN_EVENT_NAME));
                return bean;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private static class ReportMapper implements EntityMapper<Report> {

        @Override
        public Report mapRow(ResultSet rs) {
            try {
                Report report = new Report();
                report.setName(rs.getString(Fields.REPORT_NAME));
                report.setEventId(rs.getLong(Fields.REPORT_EVENT_ID));
                report.setThemeId(rs.getLong(Fields.REPORT_THEME_ID));
                return report;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}


