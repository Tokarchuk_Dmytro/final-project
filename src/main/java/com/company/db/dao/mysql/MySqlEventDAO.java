package com.company.db.dao.mysql;

import com.company.db.*;
import com.company.db.bean.EventBean;
import com.company.db.constant.DateTimeFormat;
import com.company.db.dao.EventDAO;
import com.company.db.entity.Event;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * DAO for accessing event entities
 */
public class MySqlEventDAO implements EventDAO {

    private static String QUERY_SELECT_EVENT_BEANS_WITH_PARAMETERS = "select e.*, count(r.id) as reports ,count(u.id) as users " +
            "from events as e " +
            "left join reports as r on r.event_id = e.id and r.status_id != 2  " +
            "left join users_events as ue on ue.event_id = e.id " +
            "left join users as u on u.id = ue.user_id " +
            "where date @ ? and e.language_id in ^  " +
            "group by e.id " +
            "order by # $ " +
            "limit ?,?;";

    private static String QUERY_GET_FUTURE_EVENTS_AMOUNT = "select count(events.id) as amount from  events where date >= ? and language_id in";
    private static String QUERY_GET_PAST_EVENTS_AMOUNT = "select count(events.id) as amount from  events where date < ? and language_id in";


    private static MySqlEventDAO instance;

    public static synchronized MySqlEventDAO getInstance() {
        if (instance == null) {
            instance = new MySqlEventDAO();
        }
        return instance;
    }

    private MySqlEventDAO() {
    }

    /**
     *Inserts a Event entity into MySQL database.
     *Event id generates automatically
     *
     * @param event object to be inserted
     * @throws SQLException
     */
    @Override
    public void insert(Event event) throws SQLException {
        String query = "INSERT INTO events(name,date,location,time,description,time_zone,language_id) values(?,?,?,?,?,?,?);";
        Connection con = null;
        PreparedStatement statement;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);

            statement.setString(1, event.getName());
            statement.setDate(2, new java.sql.Date(event.getDate().getTime()));
            statement.setString(3, event.getLocation());
            statement.setTime(4, Time.valueOf(event.getTime() + ":00"));
            statement.setString(5, event.getDescription());
            if (event.getTimeZone() != null) {
                statement.setString(6, event.getTimeZone());
            }
            statement.setInt(7, event.getLanguageId());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }

    }

    /**
     * Updates existing Event entity in MySQL database.
     * Requires a event id to identify existing object
     *
     * @param event object with certain {@link Event#id}
     * @throws SQLException when update fails
     */
    @Override
    public void update(Event event) throws SQLException {
        String query = "UPDATE events SET " +
                "name = ?, date = ?, location = ?, time = ?, description = ?, time_zone = ? WHERE (id = ?);";
        Connection con = null;
        PreparedStatement statement;

        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setString(1, event.getName());
            statement.setDate(2, new java.sql.Date(event.getDate().getTime()));
            statement.setString(3, event.getLocation());
            statement.setString(4, event.getTime() + ":00");
            statement.setString(5, event.getDescription());
            statement.setString(6, event.getTimeZone());
            statement.setLong(7, event.getId());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
    }

    @Override
    public void delete(Event event) throws SQLException {
        throw new UnsupportedOperationException();
    }



    /**
     * Extract a {@link Event} entity from DB
     *
     * @param id of event to be extracted
     * @return extracted event
     * @throws SQLException
     */
    @Override
    public Event getEventById(long id) throws SQLException {
        String query = "SELECT * FROM events WHERE id =?;";
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        Event event = null;
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1, id);
            rs = statement.executeQuery();
            EventMapper mapper = new EventMapper();

            while (rs.next()) {
                event = mapper.mapRow(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        return event;
    }

    /**
     * Extract a event entity list, which the user has sign up for
     *
     *
     * @param userId User id
     * @return A list of Event entity
     * @throws SQLException
     */
    @Override
    public List<Event> getEventsByUserId(long userId) throws SQLException {
        String query = "select events.* from events\n" +
                "inner join users_events on events.id = users_events.event_id\n" +
                "inner join users on users.id = users_events.user_id\n" +
                "where user_id=? " +
                "order by date desc;";
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        List<Event> events = new ArrayList<>();
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1, userId);
            rs = statement.executeQuery();
            EventMapper mapper = new EventMapper();

            while (rs.next()) {
                events.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            con.close();
        }
        return events;
    }

    /**
     * Returns a number of event entities, with specified parameters
     *
     * @param eventRelevance A event relevance. "future" of "past"
     * @param languages A list of languages id's
     * @see com.company.db.constant.Language
     * @return Amount of found event entities
     * @throws SQLException
     */

    @Override
    public int getEventsAmount(String eventRelevance, List<Integer> languages) throws SQLException {
        int amount = 0;
        String query = QUERY_GET_FUTURE_EVENTS_AMOUNT;
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        try {
            con = DBUtil.getConnection();
            if ("past".equals(eventRelevance)) {
                query = QUERY_GET_PAST_EVENTS_AMOUNT;
            }
            query+= languages.stream()
                    .map(x->Integer.toString(x))
                    .collect(Collectors.joining(",", "(", ")"))+';';
            statement = con.prepareStatement(query);
            statement.setString(1, new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date()));
            rs = statement.executeQuery();

            while (rs.next()) {
                amount = rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            con.close();
        }
        return amount;
    }



    /**
     * Extract events list, which name matches with
     * parameter string
     *
     * @param name search request string
     * @return List of founded matches
     * @throws SQLException
     */

    @Override
    public List<Event> getEventsByNameLike(String name) throws SQLException {
        String query = "SELECT * FROM events where name like ?;";
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        List<Event> events = new ArrayList<>();
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setString(1, "%" + name + "%");
            rs = statement.executeQuery();
            EventMapper mapper = new EventMapper();
            while (rs.next()) {
                events.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        return events;
    }


    /**
     * Extcract event entities list, which
     * related to user(speaker) by his id
     *
     * @param userId User(speaker)
     * @return List of events
     * @see com.company.db.constant.Role
     * @see com.company.db.entity.User
     * @throws SQLException
     */

    @Override
    public List<Event> getSpeakerEvents(long userId) throws SQLException {
        String query = "select events.* from events " +
                "join reports on events.id = reports.event_id " +
                "join users_reports on users_reports.report_id = reports.id " +
                "join users on users.id = users_reports.user_id " +
                "where users.id = ? and reports.status_id =0 " +
                "group by events.id;";
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        EventMapper mapper = new EventMapper();
        List<Event> events = new ArrayList<>();
        try {
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1, userId);
            rs = statement.executeQuery();
            while (rs.next()) {
                events.add(mapper.mapRow(rs));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        } finally {
            con.close();
        }
        return events;
    }


    /**
     *
     * Extracts events with specified parameters
     *
     * @param from query start position
     * @param range quety result amount
     * @param sortOption Sorting option. By "date","users" or"reports"
     * @param sortOrder Sorting order. "asc" or "desc"
     * @param relevance Event relevance. "future" of "past"
     * @param languages A list of event language ids
     * @see com.company.db.constant.Language
     * @see EventBeanSortManger
     * @return
     * @throws SQLException
     */

    @Override
    public List<EventBean> getEventBeansFromTo(
            int from, int range, String sortOption, String sortOrder, String relevance, List<Integer> languages) throws SQLException {
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        List<EventBean> events = new ArrayList<>();
        String languagesIds = languages.stream()
                .map(x->Integer.toString(x))
                .collect(Collectors.joining(",", "(", ")"));

        try {
            con = DBUtil.getConnection();
            statement = EventQueryManager.getSortStatement(con, sortOption, sortOrder, relevance,languagesIds);
            statement.setString(1, LocalDate.now().toString());
            statement.setInt(2, from);
            statement.setInt(3, range);
            rs = statement.executeQuery();
            EntityMapper<EventBean> mapper = new EventBeanMapper();
            while (rs.next()) {
                events.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            con.close();
        }
        return events;
    }

    /**
     * Increments a value {@link Event#usersPresence} in DB
     *
     * @param eventId Event id, which to increment
     * @throws SQLException
     */
    @Override
    public void incrementUsersPresence(long eventId) throws SQLException {
        String query = "UPDATE events as a " +
                "join events as b on b.id = ? " +
                "set a.users_presence = (b.users_presence +1) " +
                "where a.id =?;";
        Connection con = null;
        PreparedStatement statement;
        try{
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1,eventId);
            statement.setLong(2,eventId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }finally {
            con.close();
        }
    }

    /**
     * Marks user as he visited event
     * in DB
     *
     * @param userId User id
     * @param eventId Event id
     * @throws SQLException
     */

    @Override
    public void confirmUserPresence(long userId, long eventId) throws SQLException {
        String query = "UPDATE users_events SET has_visited = true WHERE (user_id = ?) and (event_id = ?);";
        Connection con = null;
        PreparedStatement statement;
        try{
            con = DBUtil.getConnection();
            statement = con.prepareStatement(query);
            statement.setLong(1,userId);
            statement.setLong(2,eventId);
            int effectedRows = statement.executeUpdate();
            if(effectedRows != 1){
                throw new SQLException("Row already effected as true");
            }

        } catch (SQLException throwables) {
            throw throwables;
        }finally {
            con.close();
        }
    }

    /**
     * Class allows to extract fields from result set
     * into event object
     *
     */
    private static class EventMapper implements EntityMapper<Event> {

        @Override
        public Event mapRow(ResultSet rs) {
            try {
                Event event = new Event();
                event.setId(rs.getLong(Fields.ID));
                event.setName(rs.getString(Fields.EVENT_NAME));
                event.setDate(rs.getDate(Fields.EVENT_DATE));
                event.setLocation(rs.getString(Fields.EVENT_LOCATION));
                event.setTime(DateTimeFormat.DEFAULT_TIME_FORMAT.formatTime(rs.getString(Fields.EVENT_TIME)));
                event.setDescription(rs.getString(Fields.EVENT_DESCRIPTION));
                event.setTimeZone(rs.getString(Fields.EVENT_TIME_ZONE));
                event.setUsersPresence(rs.getInt(Fields.EVENT_USERS_PRESENCE));
                event.setLanguageId(rs.getInt(Fields.EVENT_LANGUAGE_ID));
                return event;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Class allows to extract fields from result set
     * into event bean object
     *
     */
    private static class EventBeanMapper implements EntityMapper<EventBean> {

        @Override
        public EventBean mapRow(ResultSet rs) {
            try {
                EventBean eventBean = new EventBean();
                eventBean.setId(rs.getLong(Fields.ID));
                eventBean.setName(rs.getString(Fields.EVENT_BEAN_NAME));
                eventBean.setDate(rs.getDate(Fields.EVENT_BEAN_DATE));
                eventBean.setLocation(rs.getString(Fields.EVENT_BEAN_LOCATION));
                eventBean.setTime(DateTimeFormat.DEFAULT_TIME_FORMAT.formatTime(rs.getString(Fields.EVENT_TIME)));
                eventBean.setDescription(rs.getString(Fields.EVENT_BEAN_DESCRIPTION));
                eventBean.setTimeZone(rs.getString(Fields.EVENT_BEAN_TIME_ZONE));
                eventBean.setUsersPresence(rs.getInt(Fields.EVENT_BEAN_USERS_PRESENCE));
                eventBean.setLanguageId(rs.getInt(Fields.EVENT_BEAN_LANGUAGE_ID));
                eventBean.setSubscribers(rs.getInt(Fields.EVENT_BEAN_USERS));
                eventBean.setReports(rs.getInt(Fields.EVENT_BEAN_REPORTS));
                return eventBean;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Allows to accept sorting parameters and build DB query
     *
     */
    private static class EventQueryManager {
        public static PreparedStatement getSortStatement(Connection connection,
                                                  String sortOption, String sortOrder, String relevance, String languagesIds) throws SQLException {
            String query = MySqlEventDAO.QUERY_SELECT_EVENT_BEANS_WITH_PARAMETERS;

            query = query.replace("#",sortOption);
            query = query.replace("^",languagesIds);
            String relevanceSign;
            if("future".equals(relevance)){
                relevanceSign = ">=";
            }else {
                relevanceSign = "<";
            }
            query = query.replace("@",relevanceSign);
            query = query.replace("$",sortOrder);

            return connection.prepareStatement(query);
        }

        public PreparedStatement getSortStatement(Connection con, String sortOption, String sortOrder, String relevance) {
            return null;
        }
    }

    private static class EventBeanSortManger {

    }
}
