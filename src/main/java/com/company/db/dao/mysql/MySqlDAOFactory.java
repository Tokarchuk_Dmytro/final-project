package com.company.db.dao.mysql;


import com.company.db.dao.*;

/**
 * A realization of DAO factory for MySQL DAO objects
 */

public class MySqlDAOFactory extends DAOFactory {

    private static MySqlDAOFactory instance;

    private MySqlDAOFactory(){}

    public synchronized static DAOFactory getInstance() {
        if(instance == null){
            instance = new MySqlDAOFactory();
        }
        return instance;
    }

    @Override
    public UserDAO getUserDAO() {
        return MySqlUserDAO.getInstance();
    }

    @Override
    public ReportDAO getReportDAO() {
        return  MySqlReportDAO.getInstance();
    }

    @Override
    public EventDAO getEventDAO() {
        return MySqlEventDAO.getInstance();
    }

    @Override
    public ThemeDAO getThemeDAO() {
        return MySqlThemeDAO.getInstance();
    }
}
