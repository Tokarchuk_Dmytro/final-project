package com.company.db.constant;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Enum for Date and Time formats
 *
 */

public enum DateTimeFormat {
    DEFAULT_DATE_FORMAT(new SimpleDateFormat("yyyy-MM-dd")),
    DEFAULT_TIME_FORMAT(new SimpleDateFormat("HH:mm")),
    EXTENDED_TIME_FORMAT(new SimpleDateFormat("HH:mm:ss")),
    DEFAULT_DATE_TIME_FORMAT(new SimpleDateFormat("yyyy-MM-dd HH:mm"));

    private final SimpleDateFormat format;

    DateTimeFormat(SimpleDateFormat format){
        this.format = format;
    }

    public Date parse(String dateString) throws ParseException {
        return  format.parse(dateString);
    }

    /**
     * Formats date string
     * @param time String with new format
     * @return
     */
    public String formatTime(String time){
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }
        return format.format(date);
    }

    public Date formatTime(Date date) throws ParseException {
        String tempDate = format.format(date);
        return parse(tempDate);
    }

}
