package com.company.db.constant;

import com.company.db.entity.Report;

/**
 * Enum for reports statuses
 */

public enum Status {
    APPROVED, SUGGESTED_BY_MODERATOR, SUGGESTED_BY_USER, SUBMITTED_BY_USER;

    public static Status getRole(Report report) {
        int statusId = report.getStatusId();
        return Status.values()[statusId];
    }

    public int getId(){
        Status[]statuses = Status.values();
        for (int i=0;i<statuses.length;i++){
            if(this.equals(statuses[i])){
                return i;
            }
        }
        return -1;
    }

    public String getName() {
        return name().toLowerCase();
    }
}
