package com.company.db.constant;

import com.company.db.entity.User;
import org.apache.commons.lang3.StringUtils;

/**
 * Enum for supported languages
 */
public enum Language {
    ENGLISH("en"),UKRAINIAN("uk"),RUSSIAN("ru");


    Language(String isoCode){
        this.isoCode = isoCode;
    }

    private String isoCode;


    public static Language getLanguage(User user) {
        int roleId = user.getLanguageId();
        return Language.values()[roleId];
    }

    public int getId(){
        Language[]languages = Language.values();
        for (int i=0;i<languages.length;i++){
            if(this.equals(languages[i])){
                return i;
            }
        }
        return -1;
    }

    public String getName() {
        return StringUtils.capitalize(name().toLowerCase());
    }

    public String getIsoCode() {
        return isoCode;
    }
}
