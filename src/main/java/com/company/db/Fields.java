package com.company.db;

/**
 * DB fields names constants
 */
public class Fields {
    public static final String ID = "id";


    public static final String THEME_NAME = "name";


    //user
    public static final String USER_NAME = "name";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String USER_ROLE_ID = "role_id";
    public static final String USER_LANGUAGE_ID = "language_id";


    //report
    public static final String REPORT_NAME = "name";
    public static final String REPORT_EVENT_ID = "event_id";
    public static final String REPORT_THEME_ID = "theme_id";


    //event
    public static final String EVENT_DATE = "date";
    public static final String EVENT_NAME = "name";
    public static final String EVENT_LOCATION = "location";
    public static final String EVENT_TIME = "time";
    public static final String EVENT_DESCRIPTION = "description";
    public static final String EVENT_TIME_ZONE = "time_zone";
    public static final String EVENT_USERS_PRESENCE = "users_presence";
    public static final String EVENT_LANGUAGE_ID = "language_id";


    //report bean
    public static final String REPORT_BEAN_REPORT_ID = "report_id";
    public static final String REPORT_BEAN_THEME_ID = "theme_id";
    public static final String REPORT_BEAN_USER_ID = "user_id";
    public static final String REPORT_BEAN_STATUS_ID = "status_id";
    public static final String REPORT_BEAN_EVENT_ID = "event_id";

    public static final String REPORT_BEAN_REPORT_NAME = "report_name";
    public static final String REPORT_BEAN_THEME_NAME = "theme_name";
    public static final String REPORT_BEAN_USER_NAME = "user_name";
    public static final String REPORT_BEAN_STATUS_NAME = "status_name";
    public static final String REPORT_BEAN_EVENT_NAME = "event_name";

    //event bean

    public static final String EVENT_BEAN_DATE = "date";
    public static final String EVENT_BEAN_NAME = "name";
    public static final String EVENT_BEAN_LOCATION = "location";
    public static final String EVENT_BEAN_TIME = "time";
    public static final String EVENT_BEAN_DESCRIPTION = "description";
    public static final String EVENT_BEAN_TIME_ZONE = "time_zone";
    public static final String EVENT_BEAN_USERS_PRESENCE = "users_presence";
    public static final String EVENT_BEAN_LANGUAGE_ID = "language_id";
    public static final String EVENT_BEAN_USERS = "users";
    public static final String EVENT_BEAN_REPORTS = "reports";


}
