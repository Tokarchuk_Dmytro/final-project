package com.company.web.command.view;

import com.company.Path;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.entity.Event;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * View event form page command
 */

public class ViewEventFormCommand extends Command {

    private static final Logger logger = Logger.getLogger(ViewEventFormCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        String forward = Path.PAGE_EVENT_FORM;
        String errorMessage;
        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();
        String option = request.getParameter("option");
        Event currentEvent;
        logger.trace("Option - "+option);
        if("edit".equals(option)){
            long eventId = Long.parseLong(request.getParameter("eventId"));
            try {
                currentEvent = eventDAO.getEventById(eventId);
            } catch (SQLException throwables) {
                logger.error("Failed to edit event!",throwables);
                forward = Path.PAGE_ERROR_PAGE;
                errorMessage = Path.ERROR_NO_EVENT_FOUND;
                request.setAttribute("errorMessage", errorMessage);
                return forward;
            }
            request.setAttribute("event",currentEvent);
        }

        logger.debug("Done. Sending forward");
        request.setAttribute("option", option);
        return forward;
    }
}
