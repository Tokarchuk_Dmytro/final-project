package com.company.web.command.view;

import com.company.Path;
import com.company.db.constant.Status;
import com.company.db.bean.ReportBean;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.ReportDAO;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * View report suggestions from users command
 */

public class ViewReportSuggestionsCommand extends Command {

    private static final Logger logger = Logger.getLogger(ViewReportSuggestionsCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        String errorMessage;
        String forward = Path.PAGE_REPORT_SUGGESTIONS;
        List<ReportBean> reportBeans;

        logger.error("Getting report beans");
        try {
            reportBeans = reportDAO.getReportBeansWithStatus(Status.SUBMITTED_BY_USER,Status.SUGGESTED_BY_USER);
            logger.trace("Report beans amount - "+reportBeans.size());
        } catch (SQLException throwables) {
            logger.error("Failed to get report beans");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        request.setAttribute("reportBeans",reportBeans);

        logger.debug("Done. Sending forward");
        return forward;
    }
}
