package com.company.web.command.view;

import com.company.Path;
import com.company.db.constant.Status;
import com.company.db.bean.ReportBean;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.Event;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * View events in which user is involved
 */

public class ViewUserReportsCommand extends Command {

    private static final Logger logger = Logger.getLogger(ViewUserReportsCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        String errorMessage;
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();
        List<ReportBean> suggestedReports ;
        List<Event> events;
        String forward = Path.PAGE_SPEAKER_CABINET;
        HttpSession session = request.getSession();
        long userId = ((User)session.getAttribute("user")).getId();


        try {
            events = eventDAO.getSpeakerEvents(userId);
            suggestedReports = reportDAO.getReportBeansByUserIdWithStatus(userId,Status.SUGGESTED_BY_MODERATOR);
        } catch (SQLException | NullPointerException throwables) {
            logger.error("Failed to get events or reports " );
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        request.setAttribute("events",events);
        request.setAttribute("suggestedReports",suggestedReports);


        logger.debug("Done. Sending forward");
        return forward;
    }
}
