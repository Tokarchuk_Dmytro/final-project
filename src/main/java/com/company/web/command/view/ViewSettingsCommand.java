package com.company.web.command.view;

import com.company.Path;
import com.company.db.constant.Language;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * View settings page command
 */

public class ViewSettingsCommand  extends Command {

    private static final Logger logger = Logger.getLogger(ViewSettingsCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        HttpSession session = request.getSession();
        String forward = Path.PAGE_SETTINGS;
        int userLanguageId = ((User)session.getAttribute("user")).getLanguageId();
        List<Language> languages = new ArrayList<>(Arrays.asList(Language.values()));
        Language userLanguage = languages.remove(userLanguageId);

        logger.debug("Setting attributes");
        request.setAttribute("userLanguage",userLanguage);
        request.setAttribute("languages",languages);

        logger.debug("Done. Sending forward");
        return forward;
    }
}
