package com.company.web.command.view;

import com.company.Path;
import com.company.db.constant.DateTimeFormat;
import com.company.db.constant.Language;
import com.company.db.constant.Status;
import com.company.db.bean.ReportBean;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.Event;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * View event page command
 *
 */

public class ViewEventCommand extends Command {

    private static final Logger logger = Logger.getLogger(ViewEventCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        String errorMessage;
        List<ReportBean> reportBeans;
        Event event;
        String forward = Path.PAGE_EVENT;
        HttpSession session = request.getSession();
        long eventId;
        Language locale;
        User user = (User) session.getAttribute("user");
        try {
            eventId = Long.parseLong(request.getParameter("id"));
        }catch (NumberFormatException nfe){
            logger.error("Failed to get event or report beans",nfe);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_NO_EVENT_FOUND;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        if(user != null){
            locale = Language.getLanguage(user);
        }else {
            locale = Language.ENGLISH;

        }
        logger.debug("Getting event and report beans");
        try {
            event = eventDAO.getEventById(eventId);
            reportBeans = reportDAO.getReportBeansByEventIdWithStatus(event.getId(), locale, Status.APPROVED,Status.SUBMITTED_BY_USER,Status.SUGGESTED_BY_MODERATOR);
        } catch (SQLException | NullPointerException throwables) {
            logger.error("Failed to get event or report beans",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_NO_EVENT_FOUND;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        long exactEventTime = 0;
        try {
            exactEventTime = event.getDate().getTime()+DateTimeFormat.DEFAULT_TIME_FORMAT.parse(event.getTime()).getTime();
        } catch (ParseException e) {
            logger.warn("Failed to parse date");
        }
        request.setAttribute("exactEventTime",exactEventTime);
        request.setAttribute("reportBeans",reportBeans);
        request.setAttribute("event",event);


        logger.debug("Done. Sending forward");
        return forward;
    }
}
