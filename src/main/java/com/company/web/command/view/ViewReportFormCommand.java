package com.company.web.command.view;

import com.company.Path;
import com.company.db.constant.Language;
import com.company.db.constant.Role;
import com.company.db.bean.ReportBean;
import com.company.db.dao.*;
import com.company.db.entity.Theme;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * View report from page command
 */

public class ViewReportFormCommand extends Command {

    private static final Logger logger = Logger.getLogger(ViewReportFormCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        String forward = Path.PAGE_REPORT_FORM;
        String errorMessage;
        List<Theme> themes;
        List<User> users;

        ReportDAO reportDao = DAOFactory.getDAOFactory().getReportDAO();
        UserDAO userDAO = DAOFactory.getDAOFactory().getUserDAO();
        ThemeDAO themeDAO = DAOFactory.getDAOFactory().getThemeDAO();

        HttpSession session = request.getSession();
        Language locale = Language.getLanguage((User) session.getAttribute("user"));
        String option = request.getParameter("option");
        String eventId = request.getParameter("eventId");


        try {
            themes = themeDAO.getAllThemes(locale);
            users = userDAO.getUsersByRole(Role.SPEAKER);
        } catch (SQLException throwables) {
            logger.error("Failed to get themes or user!",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        logger.trace("Option - "+option);

        if("edit".equals(option)){
            String reportId = request.getParameter("reportId");
            try {
                ReportBean reportBean;
                reportBean = reportDao.getReportBeanById(Long.parseLong(reportId));
                request.setAttribute("report",reportBean);
                Theme currentTheme = new Theme();
                currentTheme.setId(reportBean.getThemeId());
                currentTheme.setName(reportBean.getThemeName());
                themes.remove(currentTheme);
            } catch (SQLException throwables) {
                logger.error("Failed to edit report!",throwables);
                forward = Path.PAGE_ERROR_PAGE;
                errorMessage = Path.ERROR_SOMETHING_WRONG;
                request.setAttribute("errorMessage", errorMessage);
                return forward;
            }
            request.setAttribute("reportId",reportId);
            request.setAttribute("eventId",eventId);
        }

        if("suggest".equals(option)){
            String by = request.getParameter("by");
            request.setAttribute("by",by);
        }


        request.setAttribute("speakers",users);
        request.setAttribute("eventId",eventId);
        request.setAttribute("themes",themes);
        request.setAttribute("option",option);


        logger.debug("Done. Sending forward");
        return forward;
    }
}