package com.company.web.command.view;

import com.company.Path;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * View theme form command
 */

public class ViewThemeFormCommand extends Command {

    private static final Logger logger = Logger.getLogger(ViewThemeFormCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        String forward = Path.PAGE_THEME_FORM;

        logger.debug("Done. Sending dorward");
        return forward;
    }
}
