package com.company.web.command.user;

import com.company.Path;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Command for marking event as visited
 */

public class VisitEventCommand extends Command {

    private static final Logger logger = Logger.getLogger(VisitEventCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        String forward = Path.COMMAND_SHOW_USER_EVENTS;
        String errorMessage;
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String eventId = request.getParameter("eventId");
        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();

        try {
            eventDAO.confirmUserPresence(user.getId(), Long.parseLong(eventId));
        } catch (SQLException throwables) {
            logger.debug("User already marked this event");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_ALREADY_MARKED_AS_VISITED;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        try{
            eventDAO.incrementUsersPresence(Long.parseLong(eventId));
        } catch (SQLException throwables) {
            logger.error("Failed to increment users presence in DB");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        logger.debug("Done. Sending redirect");
        response.sendRedirect(forward);
        return null;
    }
}
