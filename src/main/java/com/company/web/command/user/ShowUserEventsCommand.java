package com.company.web.command.user;

import com.company.Path;
import com.company.db.bean.ReportBean;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.Event;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * View which events the user has subscribed to
 */

public class ShowUserEventsCommand extends Command {

    private static final Logger logger = Logger.getLogger(ShowUserEventsCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();
        String errorMessage;
        List<Event> events;
        String forward = Path.PAGE_USER_EVENTS;
        HttpSession session = request.getSession();
        long userId = ((User)session.getAttribute("user")).getId();

        logger.debug("Getting events by user id");
        try {
            events = eventDAO.getEventsByUserId(userId);
        } catch (SQLException | NullPointerException throwables) {
            logger.error("Failed to get user events",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        request.setAttribute("events",events);
        request.setAttribute("currentDate", new Date());
        logger.debug("Done. Sending forward");
        return forward;
    }
}
