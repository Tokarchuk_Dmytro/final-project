package com.company.web.command.user;

import com.company.Path;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.UserDAO;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Unregister user from event
 */

public class RemoveUserEventCommand extends Command {

    private static final Logger logger = Logger.getLogger(RemoveUserEventCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Command started");
        HttpSession session = request.getSession();
        String forward = Path.COMMAND_SHOW_USER_EVENTS;
        String errorMessage ;
        long userId = ((User)session.getAttribute("user")).getId();
        String[] ids = request.getParameterValues("checkedEvents");
        if(ids == null){
            return forward;
        }
        List<Long> eventIds = Arrays.stream(ids)
                .map(Long::parseLong)
                .collect(Collectors.toList());
        UserDAO userDAO = DAOFactory.getDAOFactory().getUserDAO();



        try {
            for(Long eventId:eventIds){
                userDAO.removeUserFromEvent(userId,eventId);
            }
            logger.debug("User was removed from event(s)");
        } catch (Exception throwables) {
            logger.error("Failed to remoe user from event(s)");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage",errorMessage);
            return forward;
        }

        logger.debug("Done. Sending redirect");
        response.sendRedirect(forward);

        return null;
    }
}
