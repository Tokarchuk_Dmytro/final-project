package com.company.web.command.user;

import com.company.Path;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.UserDAO;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Register user for event command
 */

public class AddUserEventCommand extends Command {

    private static final Logger logger = Logger.getLogger(AddUserEventCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        HttpSession session = request.getSession();
        String errorMessage ;
        long userId = ((User)session.getAttribute("user")).getId();
        long eventId = Long.parseLong(request.getParameter("id"));
        String forward = Path.COMMAND_HOME;
        UserDAO userDAO = DAOFactory.getDAOFactory().getUserDAO();



        try {
            userDAO.registerUserForEvent(userId,eventId);
        } catch (Exception throwables) {
            logger.debug("User was already registered");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_ALREADY_SIGNED_UP_FOR_EVENT;
            request.setAttribute("errorMessage",errorMessage);
            return forward;
        }
        logger.debug("User was registered");

        logger.debug("Done. Sending redirect");
        response.sendRedirect(forward);
        return null;
    }
}
