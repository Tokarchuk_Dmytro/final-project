package com.company.web.command;


import com.company.web.command.user.VisitEventCommand;
import com.company.web.command.common.*;
import com.company.web.command.moderator.*;
import com.company.web.command.speaker.SubmitForSpeakerCommand;
import com.company.web.command.speaker.SuggestReportCommand;
import com.company.web.command.user.AddUserEventCommand;
import com.company.web.command.user.RemoveUserEventCommand;
import com.company.web.command.user.ShowUserEventsCommand;
import com.company.web.command.view.*;

import java.util.Map;
import java.util.TreeMap;

/**
 *  Container for commands
 * @see Command
 */


public class CommandContainer {

	private static Map<String, Command> commands = new TreeMap();
	
	static {
		commands.put("registration", new ViewRegistrationFormCommand());
		commands.put("register", new RegisterCommand());
		commands.put("login", new LoginCommand());
		commands.put("home", new HomeCommand());
		commands.put("showEvent", new ViewEventCommand());
		commands.put("singUserForEvent", new AddUserEventCommand());
		commands.put("noCommand", new NoCommand());
		commands.put("showUserEvents", new ShowUserEventsCommand());
		commands.put("removeUserEvent", new RemoveUserEventCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("moderate",new ModerateCommand());
		commands.put("addEvent",new AddEventCommand());
		commands.put("addTheme",new AddThemeCommand());
		commands.put("viewEventForm",new ViewEventFormCommand());
		commands.put("viewThemeForm",new ViewThemeFormCommand());
		commands.put("searchEvent",new SearchEventCommand());
		commands.put("editEvent", new EditEventCommand());
		commands.put("viewReportForm", new ViewReportFormCommand());
		commands.put("addReport",new AddReportCommand());
		commands.put("editReport",new EditReportCommand());
		commands.put("suggestReport", new SuggestReportCommand());
		commands.put("viewUserReports", new ViewUserReportsCommand());
		commands.put("submitForSpeaker", new SubmitForSpeakerCommand());
		commands.put("viewReportSuggestions", new ViewReportSuggestionsCommand());
		commands.put("processSuggestion", new ProcessSuggestionsCommand());
		commands.put("visitEvent", new VisitEventCommand());
		commands.put("viewSettings", new ViewSettingsCommand());
		commands.put("changeSettings", new ChangeSettingsCommand());

	}


	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			return commands.get("noCommand");
		}
		
		return commands.get(commandName);
	}
	
}