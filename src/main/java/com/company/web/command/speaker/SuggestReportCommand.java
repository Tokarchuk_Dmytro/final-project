package com.company.web.command.speaker;

import com.company.Path;
import com.company.db.constant.Status;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.Report;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Command for suggesting report by speaker
 */

public class SuggestReportCommand extends Command {

    private static final Logger logger = Logger.getLogger(SuggestReportCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting command");
        HttpSession session = request.getSession();
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        String errorMessage;
        String forward = Path.COMMAND_SHOW_EVENT;
        String eventId = request.getParameter("eventId");
        String themeId = request.getParameter("themeId");
        String by = request.getParameter("by");
        String name = request.getParameter("name");
        Report report = new Report();

        report.setName(name);
        report.setEventId(Long.parseLong(eventId));
        report.setThemeId(Long.parseLong(themeId));
        report.setStatusId(Status.SUGGESTED_BY_USER.getId());

        try {
            reportDAO.insert(report);
            User user = (User) session.getAttribute("user");
            reportDAO.setUserForReport(report.getId(),user.getId());
        } catch (SQLException throwables) {
            logger.error("Failed to insert report or set user for report");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        forward = forward+eventId;
        logger.debug("Done. Sending redirect");
        response.sendRedirect(forward);
        return null;
    }
}
