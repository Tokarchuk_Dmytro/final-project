package com.company.web.command.speaker;

import com.company.Path;
import com.company.db.constant.Status;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Command for submitting as report speaker
 */

public class SubmitForSpeakerCommand extends Command {

    private static final Logger logger = Logger.getLogger(SubmitForSpeakerCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting submit for speaker command");
        HttpSession session = request.getSession();
        String errorMessage;
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        String forward =Path.COMMAND_SHOW_EVENT;
        String eventId = request.getParameter("eventId");
        long reportId = Long.parseLong(request.getParameter("reportId"));


        try {
            User user = (User) session.getAttribute("user");
            reportDAO.updateStatus(reportId, Status.SUBMITTED_BY_USER);
            reportDAO.setUserForReport(reportId,user.getId());
            logger.debug("Report status updated. User was set for report");
        } catch (SQLException throwables) {
            logger.error("Failed to update report status or set user for report",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        logger.debug("Done. Sending redirect");
        forward+=eventId;
        response.sendRedirect(forward);

        return null;
    }
}
