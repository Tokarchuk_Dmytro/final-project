package com.company.web.command.moderator;

import com.company.Path;
import com.company.db.constant.Status;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.Report;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for editing report
 */

public class EditReportCommand extends Command {

    private static final Logger logger = Logger.getLogger(EditReportCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting edit report command");

        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        String errorMessage;
        String forward = Path.COMMAND_SHOW_EVENT;
        Report report = new Report();
        String reportId = request.getParameter("reportId");
        String name = request.getParameter("name");
        String themeId = request.getParameter("themeId");
        String eventId = request.getParameter("eventId");
        String speakerId = request.getParameter("speakerId");


        if (!nameValidation(name)){
            logger.error("Invalid data");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_INVALID_DATA;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        try {

            report.setId(Long.parseLong(reportId));
            report.setThemeId(Long.parseLong(themeId));
        } catch (NumberFormatException e) {
            logger.error("Number format exception");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        report.setName(name);
        try {
            reportDAO.update(report);
            if(!"".equals(speakerId)){
                long userId = Long.parseLong(speakerId);
                reportDAO.deleteUsersFromReportExcept(userId, Long.parseLong(reportId));
                reportDAO.setUserForReport(report.getId(), userId);
                reportDAO.updateStatus(report.getId(),Status.SUGGESTED_BY_MODERATOR);
            }
        } catch (Exception throwables) {
            logger.error("Failed to update report");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        logger.debug("Report was edited. Sending redirect");
        forward+=eventId;
        response.sendRedirect(forward);


        return null;
    }

    private boolean nameValidation(String name){
        return name.matches("^[\\p{L} ]{5,60}$");
    }
}
