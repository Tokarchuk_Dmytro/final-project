package com.company.web.command.moderator;

import com.company.Path;
import com.company.db.constant.Status;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.Report;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Process speaker and moderator suggestions for reports
 *
 */
public class ProcessSuggestionsCommand extends Command {

    private static final Logger logger = Logger.getLogger(ProcessSuggestionsCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting process suggestions command");
        String forward = Path.COMMAND_VIEW_REPORT_SUGGESTIONS;
        String errorMessage = null;
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        String option = request.getParameter("option");
        String reportId = request.getParameter("reportId");
        String userId = request.getParameter("userId");
        Report report = new Report();
        report.setId(Long.parseLong(reportId));

        logger.debug("Processing suggestion");
        //suggest
        if (userId == null) {
            // moderator approved user suggestion
            if ("approve".equals(option)) {
                try {
                    reportDAO.updateStatus(report.getId(), Status.APPROVED);
                    logger.debug("Report status was updated");
                } catch (SQLException throwables) {
                    logger.error("Failed to update report status",throwables);
                    errorMessage = Path.ERROR_SOMETHING_WRONG;
                }
            } else if ("deny".equals(option)) { // moderator denied user suggestion
                try {
                    reportDAO.delete(report);
                    logger.debug("Report was deleted");
                } catch (SQLException throwables) {
                    logger.error("Failed to delete report",throwables);
                    errorMessage = Path.ERROR_SOMETHING_WRONG;
                }
            }
        } else { //moderator approved user submit
            if ("approve".equals(option)) {
                try {
                    reportDAO.updateStatus(report.getId(), Status.APPROVED);
                    logger.debug("Report was updated");
                } catch (SQLException throwables) {
                    logger.error("Failed to update report",throwables);
                    errorMessage = Path.ERROR_SOMETHING_WRONG;
                }
            } else if ("deny".equals(option)) {  //moderator denied user submit
                try {
                    reportDAO.deleteUserFromReport(Long.parseLong(userId), report.getId());
                    reportDAO.updateStatus(report.getId(), Status.APPROVED);
                    logger.debug("Report was updated. User was deleted from report");
                } catch (SQLException throwables) {
                    logger.error("Failed to delete user from report ofr update status",throwables);
                    errorMessage = Path.ERROR_SOMETHING_WRONG;
                }

            } else if ("accept".equals(option)) {
                try {
                    reportDAO.updateStatus(report.getId(), Status.APPROVED);
                    logger.debug("Report was updated");
                } catch (SQLException throwables) {
                    logger.error("Failed to update report status");
                    errorMessage = Path.ERROR_SOMETHING_WRONG;
                }
                forward ="/home?command=viewUserReports";
            } else if ("refuse".equals(option)) {
                try {
                    reportDAO.deleteUserFromReport(Long.parseLong(userId), report.getId());
                    reportDAO.updateStatus(report.getId(), Status.APPROVED);
                    logger.debug("User was deleted from report. Report status updated");
                } catch (SQLException throwables) {
                    logger.error("Failed to update report or delete user from report");
                    errorMessage = Path.ERROR_SOMETHING_WRONG;
                }
                forward ="/home?command=viewUserReports";
            }

        }
        if(errorMessage == null){
            logger.debug("Done. Sending redirect");
            response.sendRedirect(forward);
        }else {
            forward = Path.PAGE_ERROR_PAGE;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        return null;
    }
}
