package com.company.web.command.moderator;

import com.company.Path;
import com.company.db.constant.Status;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.ReportDAO;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * View moderation page command
 */

public class ModerateCommand extends Command {

    private static final Logger logger = Logger.getLogger(ModerateCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting moderate command");
        String forward = Path.PAGE_MODERATION;
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        int suggestions= 0;

        try {
            logger.debug("Getting suggestions number");
            suggestions = reportDAO.countReportsByStatus(Status.SUGGESTED_BY_USER,Status.SUBMITTED_BY_USER);
            logger.trace("Suggestions number - "+suggestions);
        } catch (SQLException throwables) {
            logger.warn("Failed to get suggestions number!");
        }


        request.setAttribute("suggestions",suggestions);
        logger.debug("Done. Sending forward");
        return forward;
    }
}
