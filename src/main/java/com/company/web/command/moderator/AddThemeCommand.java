package com.company.web.command.moderator;

import com.company.Path;
import com.company.db.constant.Language;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.ThemeDAO;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Command for adding new theme
 */
public class AddThemeCommand extends Command {

    private static final Logger logger = Logger.getLogger(AddThemeCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting add theme command");
        ThemeDAO themeDAO = DAOFactory.getDAOFactory().getThemeDAO();
        String errorMessage;
        String forward = Path.COMMAND_MODERATE;
        String nameEn = request.getParameter("nameEn");
        String nameUa = request.getParameter("nameUa");
        String nameRu = request.getParameter("nameRu");
        Map<Language,String> names = new HashMap<>();
        names.put(Language.ENGLISH,nameEn);
        names.put(Language.UKRAINIAN,nameUa);
        names.put(Language.RUSSIAN,nameRu);
        for (Map.Entry<Language,String> entry:names.entrySet()){
            if(!validateThemeName(entry.getValue())){
                logger.error("Validation fail");
                forward = Path.PAGE_ERROR_PAGE;
                errorMessage = Path.ERROR_INVALID_DATA;
                request.setAttribute("errorMessage", errorMessage);
                return forward;
            }
        }



        try {
            themeDAO.insert(names);
        } catch (Exception throwables) {
            logger.error("Failed to add new theme",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        logger.debug("New theme was added. Sending redirect");

        response.sendRedirect(forward);

        return null;
    }

    private boolean validateThemeName(String name){
        return name.matches("^[\\p{L} ]{2,45}$");
    }
}
