package com.company.web.command.moderator;

import com.company.Path;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.entity.Event;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Command for searching events
 */

public class SearchEventCommand extends Command {

    private static final Logger logger = Logger.getLogger(SearchEventCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting search event command");
        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();
        List<Event> events;
        String errorMessage;
        String forward = Path.COMMAND_MODERATE;
        String eventNameLike = request.getParameter("eventName");


        try {
            events = eventDAO.getEventsByNameLike(eventNameLike);
        } catch (Exception throwables) {
            logger.error("Failed to get events!",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        logger.trace("Events found - "+events.size());
        request.setAttribute("events",events );
        logger.debug("Done. Sending forward");
        return forward;
    }
}
