package com.company.web.command.moderator;


import com.company.Path;
import com.company.db.dao.DAOFactory;
import com.company.db.constant.DateTimeFormat;
import com.company.db.dao.EventDAO;
import com.company.db.entity.Event;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;

/**
 * Command for adding new event
 */
public class AddEventCommand extends Command {

    private static final Logger logger = Logger.getLogger(AddEventCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting add event command");
        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();
        String errorMessage;
        String forward = Path.COMMAND_MODERATE;
        Event event = new Event();
        logger.debug("Retrieving parameters");
        String name = request.getParameter("name");
        String date = request.getParameter("date");
        String location = request.getParameter("location");
        String time = request.getParameter("time");
        String description = request.getParameter("description");
        String timeZone = request.getParameter("timeZone");
        String languageId = request.getParameter("languageId");

        event.setName(name);

        try {
            event.setDate(DateTimeFormat.DEFAULT_DATE_FORMAT.parse(date));
        } catch (ParseException e) {
            logger.error("Failed to parse date");
            errorMessage = Path.ERROR_WRONG_DATE_FORMAT;
            request.setAttribute("errorMessage",errorMessage);
            forward=Path.PAGE_ERROR_PAGE;
            return forward;
        }

        event.setLocation(location);
        event.setTime(time);
        event.setDescription(description);
        event.setTimeZone(timeZone);
        event.setLanguageId(Integer.parseInt(languageId));

        try {
            eventDAO.insert(event);
        } catch (Exception throwables) {
            logger.error("Failed to insert new event",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        logger.debug("Event was inserted. Sending redirect");

        response.sendRedirect(forward);
        return null;
    }
}
