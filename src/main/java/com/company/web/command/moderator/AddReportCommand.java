package com.company.web.command.moderator;

import com.company.Path;
import com.company.db.constant.Status;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.Report;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Command for adding new report
 */

public class AddReportCommand extends Command {

    private static final Logger logger = Logger.getLogger(AddReportCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting add report command");
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();

        String errorMessage;
        String forward = Path.COMMAND_SHOW_EVENT;
        String eventId = request.getParameter("eventId");
        String themeId = request.getParameter("themeId");
        long speakerId;
        String name = request.getParameter("name");
        Report report = new Report();
        try {
            speakerId = Long.parseLong(request.getParameter("speakerId"));
            report.setName(name);
            report.setEventId(Long.parseLong(eventId));
            report.setThemeId(Long.parseLong(themeId));
        }catch (NumberFormatException nfe){
            logger.error("Number format exception!",nfe);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }


        try {
            reportDAO.insert(report);
            if(!"".equals(speakerId)){
                reportDAO.setUserForReport(report.getId(), speakerId);
                reportDAO.updateStatus(report.getId(),Status.SUGGESTED_BY_MODERATOR);
            }
        } catch (SQLException throwables) {
            logger.error("Failed to insert event",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;

        }
        logger.debug("Report was inserted");
        forward = forward+eventId;

        response.sendRedirect(forward);
        return null;
    }
}
