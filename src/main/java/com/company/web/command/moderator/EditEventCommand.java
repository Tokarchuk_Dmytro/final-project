package com.company.web.command.moderator;

import com.company.Path;
import com.company.db.constant.DateTimeFormat;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.entity.Event;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

/**
 * Command for editing event
 */

public class EditEventCommand extends Command {

    private static final Logger logger = Logger.getLogger(EditEventCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting edit event command");

        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();
        String errorMessage;
        String forward = Path.COMMAND_HOME;
        Event event = new Event();
        logger.debug("Getting request parameters");
        long eventId;

        String name = request.getParameter("name");
        String date = request.getParameter("date");
        String location = request.getParameter("location");
        String time = request.getParameter("time");
        String description = request.getParameter("description");
        String timeZone = request.getParameter("timeZone");
        String languageId = request.getParameter("languageId");


        if(!nameValidation(name) || !locationValidation(location)){
            logger.error("Invalid data");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_INVALID_DATA;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        try {
            eventId = Long.parseLong(request.getParameter("eventId"));
        }catch (NumberFormatException nfe){
            logger.error("Number format exeption",nfe);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        try {
            event.setDate(DateTimeFormat.DEFAULT_DATE_FORMAT.parse(date));
        } catch (ParseException e) {
            logger.error("Failed to parse date",e);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_WRONG_DATE_FORMAT;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        event.setId(eventId);
        event.setName(name);
        event.setLocation(location);
        event.setTime(time);
        event.setDescription(description);
        event.setTimeZone(timeZone);
        event.setLanguageId(Integer.parseInt(languageId));

        try {
            eventDAO.update(event);
        } catch (Exception throwables) {
            logger.error("Failed to update event",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        logger.debug("Event was updated. Sending redirect");


        response.sendRedirect(forward);
        return null;
    }

    private boolean nameValidation(String name){
        return name.matches("^[\\p{L} ]{5,80}$");
    }
    private boolean locationValidation(String location){
        return location.matches("^[\\p{L} ]{5,30}$");
    }
}
