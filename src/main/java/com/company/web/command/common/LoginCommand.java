package com.company.web.command.common;

import com.company.Path;
import com.company.db.constant.Language;
import com.company.db.constant.Role;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.UserDAO;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.sql.SQLException;


/**
 * User login command
 */

public class LoginCommand extends Command {

    private static final Logger logger = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting login command");
        UserDAO userDAO = DAOFactory.getDAOFactory().getUserDAO();

        HttpSession session = request.getSession();

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        String errorMessage;
        String forward = Path.PAGE_ERROR_PAGE;

        if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
            logger.debug("Email or password empty. Sending forward to login again");
            forward = Path.PAGE_LOGIN;
            return forward;
        }

        User user = null;
        try {
            user = userDAO.findUserByEmail(email);
        } catch (SQLException throwables) {
            logger.error("Failed to get user",throwables);
        }

        if (user == null || !password.equals(user.getPassword())) {
            logger.trace("Login failed. Data dont match");
            errorMessage = Path.ERROR_NO_SUCH_USER;
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        } else {
            logger.trace("Login successful");
            Role userRole = Role.getRole(user);

            forward = null;
            response.sendRedirect(Path.COMMAND_HOME);

            session.setAttribute("user", user);
            session.setAttribute("userRole", userRole);

            logger.debug("Setting user locale");
            String userLocaleName = Language.getLanguage(user).getIsoCode();
            if (userLocaleName != null && !userLocaleName.isEmpty()) {
                Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", userLocaleName);
                session.setAttribute("defaultLocale", userLocaleName);

            }
            logger.trace("User locale - "+userLocaleName);
        }
        logger.debug("Login done. Sending redirect");
        return forward;
    }
}
