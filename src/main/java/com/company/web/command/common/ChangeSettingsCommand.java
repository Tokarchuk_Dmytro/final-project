package com.company.web.command.common;

import com.company.Path;
import com.company.db.constant.Language;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.UserDAO;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.sql.SQLException;


/**
 * Command for updating user settings
 */

public class ChangeSettingsCommand extends Command {

    private static final Logger logger = LogManager.getLogger(ChangeSettingsCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Change settings command started");
        String errorMessage;
        HttpSession session = request.getSession();
        String forward = Path.COMMAND_HOME;
        String languageId = request.getParameter("languageId");
        UserDAO userDAO = DAOFactory.getDAOFactory().getUserDAO();
        User user = (User) session.getAttribute("user");
        try {
            user.setLanguageId(Integer.parseInt(languageId));
        }catch (NumberFormatException nfe){
            logger.error("Number format exception",nfe);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage",errorMessage);
            return forward;
        }


        try {
            logger.debug("Getting user from DB");
            userDAO.update(user);
            logger.trace("User language" +Language.getLanguage(user));
            String userLocaleName = Language.getLanguage(user).getIsoCode();
            Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", userLocaleName);
            session.setAttribute("defaultLocale", userLocaleName);
        } catch (SQLException throwables) {
            logger.error("Failed to get user",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage",errorMessage);
            return forward;
        }
        logger.debug("Locale changed");
        logger.trace("User language"+Language.getLanguage(user));

        session.setAttribute("user",user);
        response.sendRedirect(forward);
        logger.debug("Sending redirect - "+forward);
        return null;
    }
}
