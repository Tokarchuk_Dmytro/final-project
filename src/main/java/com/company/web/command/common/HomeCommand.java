package com.company.web.command.common;

import com.company.Path;
import com.company.db.constant.Role;
import com.company.db.bean.EventBean;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.EventDAO;
import com.company.db.dao.ReportDAO;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *Command for showing home page
 *Implements pagination and sorting for event entities
 *
 */

public class HomeCommand extends Command {
    private  static final int pageSize = 12;
    private  static final int paginationRange = 4;

    private static final Logger logger = LogManager.getLogger(HomeCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting home command ");
        String forward = Path.PAGE_HOME;
        String errorMessage;
        List<EventBean> events;
        EventDAO eventDAO = DAOFactory.getDAOFactory().getEventDAO();
        ReportDAO reportDAO = DAOFactory.getDAOFactory().getReportDAO();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        int speakerSuggestionCount =0;

        logger.debug("Validating parameters");
        String eventRelevance = validateRelevance(request.getParameter("eventRelevance"),session);

        String sortOption = validateSortOption(request.getParameter("sortOption"),session);

        String sortOrder = validateSortOrder(request.getParameter("sortOrder"),session);

        List<Integer> languages = validateLanguages(request.getParameterValues("languages"),session);
        logger.debug("Validation done");


        if (user!=null && user.getRoleId() == Role.SPEAKER.getId()){
            try {
                logger.debug("Getting speaker suggestions count");
                speakerSuggestionCount = reportDAO.countSuggestedReportsByUserId(user.getId());
                logger.trace("Speaker suggestions - "+speakerSuggestionCount);
                session.setAttribute("speakerSuggestions",speakerSuggestionCount);
            } catch (SQLException throwables) {
                logger.warn("Failed to get speaker suggestions count",throwables);
            }
        }

        logger.debug("Starting pagination");
        int eventAmount = 0;
        int pageBegin;
        int pageEnd;
        int currentPage;
        try {
            eventAmount = eventDAO.getEventsAmount(eventRelevance,languages);
            logger.trace("Events amount - "+eventAmount);
        } catch (SQLException throwables) {
            logger.error("Failed to get events amount",throwables);
        }


        int pages = (eventAmount + pageSize - 1) / pageSize;
        try {
            currentPage = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException nfe){
            currentPage = 0;
        }


        pageBegin = currentPage - paginationRange + 2;
        pageEnd = currentPage + paginationRange;
        if( pageBegin < 1) {
            pageBegin = 1;
        }
        if(pageEnd > pages){
            pageEnd = pages;
        }
        logger.debug("Settings request attributes");


        try {
            logger.debug("Getting events");
            events = eventDAO.getEventBeansFromTo(currentPage*pageSize,pageSize,sortOption,sortOrder,eventRelevance,languages);
            request.setAttribute("events",events);
            request.setAttribute("currentPage",currentPage);
            logger.trace("Event amount -"+events.size());
            request.setAttribute("pages", pages);
            request.setAttribute("pageBegin", pageBegin);
            request.setAttribute("pageEnd", pageEnd);
            request.setAttribute("eventAmount",eventAmount);
            session.setAttribute("eventRelevance",eventRelevance);
            session.setAttribute("sortOrder",sortOrder);
            session.setAttribute("sortOption",sortOption);
        } catch (SQLException sqlException) {
            logger.error("Failed to get events!",sqlException);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage",errorMessage);

            return forward;
        }





        logger.debug("Done. Sending forward");
        return forward;
    }





    private String validateSortOrder(String sortOrder, HttpSession session) {
        if(sortOrder == null){
            sortOrder = (String) session.getAttribute("sortOrder");
            if(sortOrder == null){
                sortOrder = "asc";
                session.setAttribute("sortOrder",sortOrder);
            }
        }
        return sortOrder;
    }

    private String validateSortOption(String sortOption,HttpSession session) {
        if(sortOption == null){
            sortOption = (String) session.getAttribute("sortOption");
            if(sortOption == null){
                sortOption = "date";
                session.setAttribute("sortOption",sortOption);
            }
        }
        return sortOption;
    }

    private String validateRelevance(String relevance,HttpSession session) {
        if(relevance == null){
            relevance = (String) session.getAttribute("eventRelevance");
            if(relevance == null){
                relevance = "future";
                session.setAttribute("eventRelevance",relevance);
            }
        }
        return relevance;
    }

    private List<Integer> validateLanguages(String[] languages, HttpSession session){
        List<Integer> languagesIds;
        if(languages != null){
            try {
                languagesIds = Arrays.stream(languages)
                        .map(Integer::parseInt)
                        .collect(Collectors.toList());
            }catch (NumberFormatException nfe){
                logger.error("Number format exception",nfe);
                languagesIds = new ArrayList<>(Arrays.asList(0,1,2));
            }
        } else {
            List<Long> longList = (List<Long>) session.getAttribute("eventLanguages");
            if(longList == null){
                languagesIds = new ArrayList<>(Arrays.asList(0,1,2));
            }else {
                languagesIds = longList.stream().map(Long::intValue).collect(Collectors.toList());
            }
        }
        session.setAttribute("eventLanguages",languagesIds.stream().map(Integer::longValue).collect(Collectors.toList()));
        return  languagesIds;
    }


}
