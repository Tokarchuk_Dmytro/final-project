package com.company.web.command.common;

import com.company.Path;
import com.company.db.dao.DAOFactory;
import com.company.db.dao.UserDAO;
import com.company.db.entity.User;
import com.company.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * User registration command
 */

public class RegisterCommand extends Command {

    private static final Logger logger = Logger.getLogger(RegisterCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("Starting register command");
        String forward = Path.PAGE_LOGIN;
        String errorMessage;
        UserDAO userDAO = DAOFactory.getDAOFactory("mysql").getUserDAO();
        String name = request.getParameter("fname");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String passwordConf = request.getParameter("password_conf");
        String languageId = request.getParameter("language");

        logger.debug("Starting parameters validation");
        if (validateName(name) || validatePassword(password,passwordConf) || validateEmail(email)){
            logger.debug("Validation failed. Sending redirect");
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage =Path.ERROR_REGISTRATION_VALIDATION;
            request.setAttribute("errorMessage",errorMessage);
            return forward;
        }

        logger.debug("Validation done");
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        user.setLanguageId(Integer.parseInt(languageId));
        user.setRoleId(2);

        try {
            if(userDAO.isUserEmailExist(email)){
                logger.debug("User with thid email already exist. Sending redirect");
                forward = Path.PAGE_ERROR_PAGE;
                errorMessage = Path.ERROR_USER_ALREADY_EXISTS;
                request.setAttribute("errorMessage",errorMessage);
                return forward;
            }
            userDAO.insert(user);
            logger.debug("User added");
        } catch (SQLException throwables) {
            logger.error("Failed to ge user!",throwables);
            forward = Path.PAGE_ERROR_PAGE;
            errorMessage = Path.ERROR_SOMETHING_WRONG;
            request.setAttribute("errorMessage",errorMessage);
            return forward;
        }


        return forward;
    }

    private boolean validateName(String name) {
        if (!name.matches("^\\p{L}{2,20} \\p{L}{2,20}[ ]{0,20}$")) {
            return true;
        }
        if (name.length() > 40) {
            return true;
        }
        name = name.trim();
        return false;
    }

    private boolean validatePassword(String password, String passwordConf) {
        if(!password.equals(passwordConf)){
            return false;
        }
        return !password.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,20}$");
    }

    private boolean validateEmail(String email){
        return !email.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
    }


}
