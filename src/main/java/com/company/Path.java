package com.company;

/**
 * String constants for pages,commands and resources bundles properties
 */
public class Path {

    //pages
    public static final String PAGE_EVENT = "WEB-INF/jsp/common/event.jsp";
    public static final String PAGE_HOME = "WEB-INF/jsp/common/home.jsp";
    public static final String PAGE_LOGIN = "WEB-INF/jsp/common/login.jsp";
    public static final String PAGE_REGISTER = "WEB-INF/jsp/common/register.jsp";
    public static final String PAGE_SETTINGS = "WEB-INF/jsp/common/settings.jsp";
    public static final String PAGE_EVENT_FORM = "WEB-INF/jsp/from/event_form.jsp";
    public static final String PAGE_REPORT_FORM = "WEB-INF/jsp/from/report_form.jsp";
    public static final String PAGE_THEME_FORM = "WEB-INF/jsp/from/theme_form.jsp";
    public static final String PAGE_MODERATION = "WEB-INF/jsp/moderator/moderation.jsp";
    public static final String PAGE_REPORT_SUGGESTIONS = "WEB-INF/jsp/moderator/report_suggestions.jsp";
    public static final String PAGE_SPEAKER_CABINET = "WEB-INF/jsp/speaker/speaker_cabinet.jsp";
    public static final String PAGE_USER_EVENTS = "WEB-INF/jsp/user/user_events.jsp";
    public static final String PAGE_ERROR_PAGE = "WEB-INF/jsp/error_page.jsp";

    //commands
    public static final String COMMAND_HOME = "/home?command=home";
    public static final String COMMAND_MODERATE = "/home?command=moderate";
    public static final String COMMAND_SHOW_EVENT = "/home?command=showEvent&id=";
    public static final String COMMAND_VIEW_REPORT_SUGGESTIONS = "/home?command=viewReportSuggestions";
    public static final String COMMAND_SHOW_USER_EVENTS = "/home?command=showUserEvents";

    //errors
    public static final String ERROR_NO_SUCH_COMMAND = "error.no_such_command";
    public static final String ERROR_ACCESS_DENIED = "error.access_denied";
    public static final String ERROR_NO_SUCH_USER = "error.no_user_with_such_email_or_password" ;
    public static final String ERROR_REGISTRATION_VALIDATION = "error.registration_validation" ;
    public static final String ERROR_USER_ALREADY_EXISTS = "error.user_already_exists" ;
    public static final String ERROR_SOMETHING_WRONG = "error.something_wrong" ;
    public static final String ERROR_WRONG_DATE_FORMAT = "error.wrong_date_format" ;
    public static final String ERROR_ALREADY_SIGNED_UP_FOR_EVENT = "error.already_signed_up_for_event" ;
    public static final String ERROR_ALREADY_MARKED_AS_VISITED = "error.already_marked_as_visited" ;
    public static final String ERROR_NO_EVENT_FOUND = "error.no_event_found" ;
    public static final String ERROR_INVALID_DATA = "error.valid_data" ;


}
