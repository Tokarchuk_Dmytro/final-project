<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.text.*" %>
<html lang="en">
<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container-sm">
    <form>
        <div class="form-group row">
            <div class="form-group">
                <label for="emailIn">Email</label>
                <input type="email" class="form-control" name="emailIn" id="emailIn" aria-describedby="emailHelpId"
                       placeholder="Enter email">
            </div>
        </div>
        <fieldset class="form-group row">
            <div class="form-group">
                <label for="passwordIn">Password</label>
                <input type="password" class="form-control" name="passwordIn" id="passwordIn"
                       placeholder="Enter password">
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>

<div class="container-sm">
    <h1>My First Bootstrap Page</h1>
    <p>This is some text.</p>
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
    </tr>
    <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
    </tr>
    <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
    </tr>
    </tbody>
</table>







</body>
</html>
