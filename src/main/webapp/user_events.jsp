<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My events</title>
</head>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

This is yours events ${user.name}<br/><br/><br/>




    <c:choose>
        <c:when test="${empty events}">
            You haven't registered for any conference yet<br/>
        </c:when>
        <c:otherwise>
            <form method="get" action="home">
                 <input type="submit" value="Unregister from selected"/><br/>
                 <input type="hidden" name="command" value="removeUserEvent">

                <jsp:useBean id="today" class="java.util.Date"/>
                <c:forEach var="event" items="${events}">
                     <a href="/home?command=showEvent&id=${event.id}"> ${event.name} </a>
                         <c:if test="${event.date gt today}">
                             <input type="checkbox" name="checkedEvents" value="${event.id}">

                         </c:if>

                     <div>
                             ${event.date} ${event.time}
                     </div>

                 </c:forEach>
            </form>
            <c:forEach var="event" items="${events}">
                    <c:if test="${event.date gt today}">
                        <form method="get" action="home">
                            <a href="/home?command=showEvent&id=${event.id}">${event.name}</a><br/>
                            <input type="submit" value="Mark as visited"/>
                            <input type="hidden" name="command" value="visitEvent">
                            <input type="hidden" name="eventId" value="${event.id}"/>
                        ${event.date} ${event.time}
                        </form>
                        <div>
                        </div>
                    </c:if>
                </c:forEach>
        </c:otherwise>
    </c:choose>



</body>
</html>
