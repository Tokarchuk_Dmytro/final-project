<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="Suggestions"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>


<div class="container-fluid" style="margin-top:90px">

    <table class="table table-bordered">
        <thead class="table-primary">
        <tr>
            <th class="tg-c3ow"><fmt:message key="report_suggestions_jsp.table.speaker"/></th>
            <th class="tg-c3ow"><fmt:message key="report_suggestions_jsp.table.action"/></th>
            <th class="tg-c3ow"><fmt:message key="report_suggestions_jsp.table.report"/></th>
            <th class="tg-c3ow"><fmt:message key="report_suggestions_jsp.table.theme"/></th>
            <th class="tg-c3ow"><fmt:message key="report_suggestions_jsp.table.event"/></th>
            <th class="tg-c3ow"><fmt:message key="report_suggestions_jsp.table.option"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="report" items="${reportBeans}">
            <tr>
                <td class="tg-0pky">${report.userName}</td>
                <td class="tg-c3ow">
                    <c:choose>
                        <c:when test="${report.statusId == 2}">
                            <fmt:message key="report_suggestions_jsp.action.suggested"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="report_suggestions_jsp.action.submitted_for"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td class="tg-c3ow">${report.reportName} </td>
                <td class="tg-c3ow">${report.themeName} </td>
                <td class="tg-c3ow"><a href="/home?command=showEvent&id=${report.eventId}"> ${report.eventName} </a>
                </td>
                <td class="tg-c3ow">

                    <c:choose>
                        <c:when test="${report.statusId == 2}">
                            <form action="/home" method="post">
                                <input type="hidden" name="command" value="processSuggestion"/>
                                <input type="hidden" name="option" value="approve"/>
                                <input type="hidden" name="reportId" value="${report.reportId}">
                                <button type="submit" class="btn btn-success"><fmt:message key="report_suggestions_jsp.button.approve"/></button>
                            </form>
                            <form action="/home" method="post">
                                <input type="hidden" name="command" value="processSuggestion"/>
                                <input type="hidden" name="option" value="deny"/>
                                <input type="hidden" name="reportId" value="${report.reportId}">
                                <button type="submit" class="btn btn-danger"><fmt:message key="report_suggestions_jsp.button.deny"/></button>
                            </form>
                        </c:when>
                        <c:otherwise>
                                <form action="/home" method="post">
                                    <input type="hidden" name="command" value="processSuggestion"/>
                                    <input type="hidden" name="option" value="approve"/>
                                    <input type="hidden" name="reportId" value="${report.reportId}">
                                    <input type="hidden" name="userId" value="${report.userId}">
                                    <button type="submit" class="btn btn-success"><fmt:message key="report_suggestions_jsp.button.approve"/></button>
                                </form>
                                <form action="/home" method="post">
                                    <input type="hidden" name="command" value="processSuggestion"/>
                                    <input type="hidden" name="option" value="deny"/>
                                    <input type="hidden" name="reportId" value="${report.reportId}">
                                    <input type="hidden" name="userId" value="${report.userId}">
                                    <button type="submit" class="btn btn-danger"><fmt:message key="report_suggestions_jsp.button.deny"/></button>
                                </form>
                        </c:otherwise>
                    </c:choose>

                </td>

            </tr>
        </c:forEach>
        </tbody>
    </table>


</div>


<%--<div>--%>

<%--    <form method="post" action="home">--%>


<%--        <br/>--%>
<%--        <c:forEach var="report" items="${reportBeans}">--%>
<%--            <c:choose>--%>
<%--                <c:when test="${report.statusId == 2}">--%>
<%--                    <span style="align-items: start">--%>

<%--                        <a>${report.userName} suggested a report "${report.reportName}" theme: ${report.themeName} for event </a>--%>
<%--                        <a href="/home?command=showEvent&id=${report.eventId}"> ${report.eventName} </a>--%>
<%--                        <div>--%>
<%--                        <form action="/home" method="post">--%>
<%--                            <input type="hidden" name="command" value="processSuggestion"/>--%>
<%--                            <input type="hidden" name="option" value="approve"/>--%>
<%--                            <input type="hidden" name="reportId" value="${report.reportId}">--%>
<%--                            <input type="submit" value="Approve"/>--%>
<%--                        </form>--%>
<%--                        </div>--%>
<%--                        <div>--%>
<%--                        <form action="/home" method="post">--%>
<%--                            <input type="hidden" name="command" value="processSuggestion"/>--%>
<%--                            <input type="hidden" name="option" value="deny"/>--%>
<%--                            <input type="hidden" name="reportId" value="${report.reportId}">--%>
<%--                            <input type="submit" value="Deny"/>--%>
<%--                        </form>--%>
<%--                            </div>--%>
<%--                    </span>--%>

<%--                </c:when>--%>
<%--                <c:otherwise>--%>
<%--                    <span style="align-items: start">--%>
<%--                    <a>${report.userName} submited for "${report.reportName}" theme: ${report.themeName} of event </a>--%>
<%--                    <a href="/home?command=showEvent&id=${report.eventId}"> ${report.eventName} </a>--%>
<%--                        <div>--%>
<%--                            <form action="/home" method="post">--%>
<%--                            <input type="hidden" name="command" value="processSuggestion"/>--%>
<%--                            <input type="hidden" name="option" value="approve"/>--%>
<%--                            <input type="hidden" name="reportId" value="${report.reportId}">--%>
<%--                            <input type="hidden" name="userId" value="${report.userId}">--%>
<%--                            <input type="submit" value="Approve"/>--%>
<%--                        </form>--%>
<%--                        </div>--%>
<%--                        <div>--%>
<%--                            <form action="/home" method="post">--%>
<%--                            <input type="hidden" name="command" value="processSuggestion"/>--%>
<%--                            <input type="hidden" name="option" value="deny"/>--%>
<%--                            <input type="hidden" name="reportId" value="${report.reportId}">--%>
<%--                            <input type="hidden" name="userId" value="${report.userId}">--%>
<%--                            <input type="submit" value="Deny"/>--%>
<%--                            </form>--%>
<%--                        </div>--%>
<%--                    </span>--%>
<%--                </c:otherwise>--%>
<%--            </c:choose>--%>

<%--        </c:forEach>--%>

<%--    </form>--%>

<%--</div>--%>


</body>
</html>
