<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="Login"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid" style="margin: 90px">
    <h2 class="display-4"><fmt:message key="moderation_jsp.h.moderator_menu"/></h2>

    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-2">
            <form action="/home" method="get">
                <input type="hidden" name="command" value="viewEventForm"/>
                <input type="hidden" name="option" value="new">
                <button type="submit" class="btn btn-primary btn-sm"><fmt:message key="moderation_jsp.button.add_event"/></button>
            </form>
        </div>
        <div class="col-sm-2">
            <form action="/home" method="get">
                <input type="hidden" name="command" value="viewThemeForm"/>
                <button type="submit" class="btn btn-primary btn-sm"><fmt:message key="moderation_jsp.button.add_theme"/></button>
            </form>
        </div>
        <div class="col-sm-2">
            <form action="/home" method="get">
                <input type="hidden" name="command" value="viewReportSuggestions"/>
                <button type="submit" class="btn btn-primary btn-sm">
                    <fmt:message key="moderation_jsp.button.check_suggestions"/> <span class="badge badge-light">${suggestions}</span>
                </button>
            </form>
        </div>
        <div class="col-sm-4"></div>
        <div class="container mx-4">
            <form action="/home" method="get">
                <div class="input-group mb-3">
                    <fmt:message key="moderation_jsp.placeholder.enter_event_name" var="placeholder_event_name"/>
                    <input type="hidden" name="command" value="searchEvent"/>
                    <input type="text" class="form-control" name="eventName" placeholder="${placeholder_event_name}">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"><fmt:message key="moderation_jsp.button.search"/></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <c:if test="${not empty events}">
        <table class="table table-bordered">
            <thead class="table-primary">
            <tr>
                <th class="tg-c3ow"><fmt:message key="moderation_jsp.table.name"/></th>
                <th class="tg-c3ow"><fmt:message key="moderation_jsp.table.date"/></th>
                <th class="tg-c3ow"><fmt:message key="moderation_jsp.table.time"/></th>
                <th class="tg-c3ow"><fmt:message key="moderation_jsp.table.location"/></th>
                <th class="tg-c3ow"><fmt:message key="moderation_jsp.table.time_zone"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="event" items="${events}">
                <tr>
                    <td class="tg-0pky"><a class="aStyle"
                                           href="/home?command=showEvent&id=${event.id}"> ${event.name} </a>
                    </td>
                    <td class="tg-c3ow">${event.date}</td>
                    <td class="tg-c3ow">${event.time}</td>
                    <td class="tg-c3ow">${event.location}</td>
                    <td class="tg-c3ow">${event.timeZone}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>


</body>
</html>
