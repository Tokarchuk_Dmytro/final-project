<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="My events"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<jsp:useBean id="today" class="java.util.Date"/>


<div class="container" style="margin-top:100px">

    <div class="row">
        <div class="col-sm-6">
            <h1><fmt:message key="user_events_jsp.h.my_future_events"/></h1>
            <c:choose>
            <c:when test="${not empty events}">
            <form method="get" action="home">
                <input type="hidden" name="command" value="removeUserEvent">
                <table class="table table-striped">
                    <thead class="table-secondary">
                    <tr>
                        <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.name"/></th>
                        <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.date"/></th>
                        <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.time"/></th>
                        <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.location"/></th>
                        <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.time_zone"/></th>
                        <td class="tg-c3ow"><fmt:message key="user_events_jsp.table.option"/></td>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="event" items="${events}">
                        <c:if test="${event.date gt today}">

                        <tr>
                            <td class="tg-0pky"><a href="/home?command=showEvent&id=${event.id}"> ${event.name}</a></td>
                            <td class="tg-c3ow">${event.date}</td>
                            <td class="tg-c3ow">${event.time}</td>
                            <td class="tg-c3ow">${event.location}</td>
                            <td class="tg-c3ow">${event.timeZone}</td>
                            <td class="tg-c3ow">
                                <div class="mx-auto">
                                <input type="checkbox" class="form-check-input" name="checkedEvents" value="${event.id}">
                                </div>
                            </td>
                        </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
                <fmt:message key="user_events_jsp.button.unregister_from_selected" var="button_unregister_from_selected"/>
                <input type="submit" class="btn btn-primary" value="${button_unregister_from_selected}"/>
            </form>
            </c:when>
                </c:choose>

        </div>
        <div class="col-sm-6">

            <h1><fmt:message key="user_events_jsp.h.my_passed_events"/></h1>
            <table class="table table-striped">
                <thead class="table-secondary">
                <tr>
                    <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.name"/></th>
                    <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.date"/></th>
                    <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.time"/></th>
                    <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.location"/></th>
                    <th class="tg-c3ow"><fmt:message key="user_events_jsp.table.time_zone"/></th>
                    <td class="tg-c3ow"><fmt:message key="user_events_jsp.table.option"/></td>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="event" items="${events}">
                    <c:if test="${event.date lt today}">

                        <tr>
                            <td class="tg-0pky"><a href="/home?command=showEvent&id=${event.id}"> ${event.name}</a></td>
                            <td class="tg-c3ow">${event.date}</td>
                            <td class="tg-c3ow">${event.time}</td>
                            <td class="tg-c3ow">${event.location}</td>
                            <td class="tg-c3ow">${event.timeZone}</td>
                            <td class="tg-c3ow">
                                <fmt:message key="user_events_jsp.button.mark_as_visited" var="button_mark_as_visited"/>
                                <form method="get" action="home">
                                    <input type="submit" class="btn btn-primary" value="${button_mark_as_visited}"/>
                                    <input type="hidden" name="command" value="visitEvent">
                                    <input type="hidden" name="eventId" value="${event.id}"/>
                                </form>
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>

        </div>

    </div>
</div>

</body>
</html>