<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="Login"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid" style="margin-top:90px">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-6">
                <h1><fmt:message key="speaker_cabinet_jsp.h.event_history"/></h1>
                <c:if test="${empty events}">
                    <p class="text-info"><fmt:message key="speaker_cabinet_jsp.p.empty_history"/></p>
                </c:if>
                <c:if test="${not empty events}">
                    <table class="table table-bordered">
                        <thead class="table-primary">
                        <tr>
                            <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.name"/></th>
                            <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.date"/></th>
                            <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.time"/></th>
                            <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.location"/></th>
                            <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.time_zone"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="event" items="${events}">
                            <tr>
                                <td class="tg-0pky"><a class="aStyle"
                                                       href="/home?command=showEvent&id=${event.id}"> ${event.name} </a>
                                </td>
                                <td class="tg-c3ow">${event.date}</td>
                                <td class="tg-c3ow">${event.time}</td>
                                <td class="tg-c3ow">${event.location}</td>
                                <td class="tg-c3ow">${event.timeZone}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:if>
            </div>
            <div class="col-sm-6">
                <h1><fmt:message key="speaker_cabinet_jsp.h.suggestions"/></h1>
                <table class="table table-bordered">
                    <thead class="table-primary">
                    <tr>
                        <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.message"/></th>
                        <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.report"/></th>
                        <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.theme"/></th>
                        <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.event"/></th>
                        <th class="tg-c3ow"><fmt:message key="speaker_cabinet_jsp.table.option"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="report" items="${suggestedReports}">
                        <tr>
                            <td class="tg-0pky"><p class="text-primary"><fmt:message key="speaker_cabinet_jsp.message"/></p></td>
                            <td class="tg-c3ow">${report.reportName}</td>
                            <td class="tg-c3ow">${report.themeName}</td>
                            <td class="tg-c3ow">
                                <a class="aStyle" href="/home?command=showEvent&id=${report.eventId}"> ${report.eventName} </a>
                            </td>
                            <td class="tg-c3ow">
                                <div>
                                    <form action="/home" method="post">
                                        <input type="hidden" name="command" value="processSuggestion"/>
                                        <input type="hidden" name="option" value="accept"/>
                                        <input type="hidden" name="reportId" value="${report.reportId}">
                                        <input type="hidden" name="userId" value="${report.userId}">
                                        <button type="submit" class="btn btn-success"><fmt:message key="speaker_cabinet_jsp.button.accept"/></button>
                                    </form>
                                </div>
                                <div>
                                    <form action="/home" method="post">
                                        <input type="hidden" name="command" value="processSuggestion"/>
                                        <input type="hidden" name="option" value="refuse"/>
                                        <input type="hidden" name="reportId" value="${report.reportId}">
                                        <input type="hidden" name="userId" value="${report.userId}">
                                        <button type="submit" class="btn btn-danger"><fmt:message key="speaker_cabinet_jsp.button.refuse"/></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>


            </div>

        </div>


    </div>


    <%--<c:choose>--%>
    <%--    <c:when test="${empty reportBeans}">--%>
    <%--        You doesn't have any reports yet<br/>--%>
    <%--    </c:when>--%>
    <%--    <c:otherwise>--%>
    <%--        <form method="get" action="home">--%>
    <%--            <input type="submit" value="Unregister from selected"/><br/>--%>
    <%--            <input type="hidden" name="command" value="removeUserReport">--%>

    <%--            <c:forEach var="report" items="${reportBeans}">--%>
    <%--                <a href="/home?command=showEvent&id="> ${report.reportName} </a>--%>
    <%--                <input type="checkbox" name="checkedEvents" value="">--%>
    <%--                <div>--%>
    <%--                        ${report.statusName} ${report.themeName}--%>
    <%--                </div>--%>

    <%--            </c:forEach>--%>
    <%--        </form>--%>
    <%--    </c:otherwise>--%>
    <%--</c:choose>--%>

    <%--<c:choose>--%>
    <%--    <c:when test="${empty events}">--%>
    <%--        You doesn't have any reports yet<br/>--%>
    <%--    </c:when>--%>
    <%--    <c:otherwise>--%>
    <%--            <c:forEach var="event" items="${events}">--%>
    <%--                <a href="/home?command=showEvent&id=${event.id}"> ${event.name} </a>--%>
    <%--                <div>--%>
    <%--                        ${event.date}  ${ event.time}  ${event.location}--%>
    <%--                </div>--%>

    <%--            </c:forEach>--%>
    <%--    </c:otherwise>--%>
    <%--</c:choose>--%>
    <%--<br/>--%>
    <%--<br/>--%>


    <%--    <c:forEach var="report" items="${suggestedReports}">--%>
    <%--        <div>--%>
    <%--            You hae a suggestion to run report named ${report.reportName} of event--%>
    <%--            <a href="/home?command=showEvent&id=${report.eventId}"> ${report.eventName} </a>--%>
    <%--            <div>--%>
    <%--                <form action="/home" method="post">--%>
    <%--                    <input type="hidden" name="command" value="processSuggestion"/>--%>
    <%--                    <input type="hidden" name="option" value="accept"/>--%>
    <%--                    <input type="hidden" name="reportId" value="${report.reportId}">--%>
    <%--                    <input type="hidden" name="userId" value="${report.userId}">--%>
    <%--                    <input type="submit" value="Accept"/>--%>
    <%--                </form>--%>
    <%--            </div>--%>
    <%--            <div>--%>
    <%--                <form action="/home" method="post">--%>
    <%--                    <input type="hidden" name="command" value="processSuggestion"/>--%>
    <%--                    <input type="hidden" name="option" value="refuse"/>--%>
    <%--                    <input type="hidden" name="reportId" value="${report.reportId}">--%>
    <%--                    <input type="hidden" name="userId" value="${report.userId}">--%>
    <%--                    <input type="submit" value="Refuse"/>--%>
    <%--                </form>--%>
    <%--            </div>--%>
    <%--        </div>--%>

    <%--    </c:forEach>--%>

</div>
</body>
</html>
