<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="Report form"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container shadow-lg p-3 mb-5 bg-white rounded" style="margin-top:160px;width: 600px">
    <div class="mx-auto" style="width:500px">
        <h2><fmt:message key="report_form_jsp.h.report_form"/></h2>
        <form action="home" method="post">
            <c:choose>
                <c:when test="${option == 'add'}">
                    <input type="hidden" name="command" value="addReport"/>
                    <input type="hidden" name="eventId" value="${eventId}"/>
                </c:when>
                <c:when test="${option == 'suggest'}">
                    <input type="hidden" name="command" value="suggestReport"/>
                    <input type="hidden" name="eventId" value="${eventId}">
                    <input type="hidden" name="by" value="${by}">
                </c:when>
                <c:when test="${option == 'edit'}">
                    <input type="hidden" name="command" value="editReport"/>
                    <input type="hidden" name="reportId" value="${reportId}">
                    <input type="hidden" name="eventId" value="${eventId}">
                </c:when>
            </c:choose>

            <div class="form-group">
                <fmt:message key="report_form_jsp.placeholder.name" var="placeholder_name"/>
                <label for="name"><fmt:message key="report_form_jsp.label.name"/></label>
                <input type="text" class="form-control" id="name" placeholder="${placeholder_name}" name="name"
                       <c:if test="${report != null}">value="${report.reportName}"</c:if>/>
            </div>

            <c:if test="${option eq 'edit' || option eq 'add'}">
            <div class="form-group">
                <fmt:message key="report_form_jsp.placeholder.speaker.none" var="none_speaker"/>
                <label for="speakerSelect"><fmt:message key="report_form_jsp.label.speaker"/></label>
                <select class="form-control" id="speakerSelect" name="speakerId">
                    <option value="${null}">${none_speaker}</option>
                    <c:forEach items="${speakers}" var="speaker">
                        <option value="${speaker.id}">${speaker.name}</option>
                    </c:forEach>
                </select>
            </div>
            </c:if>
            <div class="form-group">
                <label for="themeSelect"><fmt:message key="report_form_jsp.label.theme"/></label>
                <select class="form-control" id="themeSelect" name="themeId">
                    <c:if test="${option == 'edit'}">
                        <option value="${report.themeId}">${report.themeName} </option>
                    </c:if>
                    <c:forEach var="theme" items="${themes}">
                        <option value="${theme.id}"> ${theme.name} </option>
                    </c:forEach>
                </select>
            </div>
            <div class="text-center">
                <c:choose>
                    <c:when test="${option == 'edit'}">
                        <button type="submit" class="btn btn-primary"><fmt:message key="report_form_jsp.button.edit"/></button>
                    </c:when>
                    <c:when test="${option == 'suggest'}">
                        <button type="submit" class="btn btn-primary"><fmt:message key="report_form_jsp.button.suggest"/></button>
                    </c:when>
                    <c:when test="${option == 'add'}">
                        <button type="submit" class="btn btn-primary"><fmt:message key="report_form_jsp.button.crate"/></button>
                    </c:when>
                </c:choose>
            </div>
        </form>
    </div>

</div>


<%--This is a new report form<br/><br/>--%>
<%--<form action="home" method="post">--%>
<%--    <c:choose>--%>
<%--        <c:when test="${option == 'add'}">--%>
<%--            <input type="hidden" name="command" value="addReport"/>--%>
<%--            <input type="hidden" name="eventId" value="${eventId}"/>--%>
<%--        </c:when>--%>
<%--        <c:when test="${option == 'suggest'}">--%>
<%--            <input type="hidden" name="command" value="suggestReport"/>--%>
<%--            <input type="hidden" name="eventId" value="${eventId}">--%>
<%--            <input type="hidden" name="by" value="${by}">--%>
<%--        </c:when>--%>
<%--        <c:when test="${option == 'edit'}">--%>
<%--            <input type="hidden" name="command" value="editReport"/>--%>
<%--            <input type="hidden" name="reportId" value="${reportId}">--%>
<%--            <input type="hidden" name="eventId" value="${eventId}">--%>
<%--        </c:when>--%>
<%--    </c:choose>--%>


<%--    <label for="name">Name:</label>--%>
<%--    <input placeholder="Enter report name" type="text" id="name" name="name"--%>
<%--    <c:if test="${report != null}">--%>
<%--           value="${report.reportName}"--%>
<%--    </c:if>--%>
<%--    ><br><br>--%>
<%--    <c:if test="${option =='edit' || option == 'add'}">--%>
<%--        <label for="speakerSelect">Select a speaker</label>--%>
<%--        <select id="speakerSelect" name="speakerId">--%>
<%--            <option value="${null}"> None</option>--%>
<%--            <c:forEach items="${speakers}" var="speaker">--%>
<%--                <option value="${speaker.id}">${speaker.name}</option>--%>
<%--            </c:forEach>--%>
<%--        </select>--%>

<%--    </c:if>--%>

<%--    <label for="reportSelect">Choose theme:</label>--%>
<%--    <select id="reportSelect" name="themeId">--%>
<%--        <c:if test="${option == 'edit'}">--%>
<%--            <option value="${report.themeId}">${report.themeName} </option>--%>
<%--        </c:if>--%>
<%--        <c:forEach var="theme" items="${themes}">--%>
<%--            <option value="${theme.id}"> ${theme.name} </option>--%>
<%--        </c:forEach>--%>
<%--    </select><br/><br/>--%>

<%--    <c:choose>--%>
<%--        <c:when test="${option == 'edit'}">--%>
<%--            <input type="submit" value="Edit"/>--%>
<%--        </c:when>--%>
<%--        <c:when test="${option == 'suggest'}">--%>
<%--            <input type="submit" value="Suggest"/>--%>
<%--        </c:when>--%>
<%--        <c:when test="${option == 'add'}">--%>
<%--            <input type="submit" value="Create"/>--%>
<%--        </c:when>--%>
<%--    </c:choose>--%>
<%--</form>--%>


</body>
</html>
