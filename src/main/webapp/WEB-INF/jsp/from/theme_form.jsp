<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="New theme"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container shadow-lg p-3 mb-5 bg-white rounded" style="margin-top:160px;width: 600px">
    <div class="mx-auto" style="width:500px">
        <h2><fmt:message key="theme_form_jsp.h.theme_form"/></h2>
        <form action="home" method="post">
                <input type="hidden" name="command" value="addTheme"/>
            <div class="form-group">
                <fmt:message key="theme_form_jsp.placeholder.name_en" var="placeholder_name_en"/>
                <label for="nameEn"><fmt:message key="theme_form_jsp.label.name_en"/></label>
                <input type="text" class="form-control" id="nameEn" placeholder="${placeholder_name_en}" name="nameEn" minlength="4">
            </div>
            <div class="form-group">
                <fmt:message key="theme_form_jsp.placeholder.name_ua" var="placeholder_name_ua"/>
                <label for="nameUa"><fmt:message key="theme_form_jsp.label.name_ua"/></label>
                <input type="text" class="form-control" id="nameUa" placeholder="${placeholder_name_ua}" name="nameUa" minlength="4">
            </div>
            <div class="form-group">
                <fmt:message key="theme_form_jsp.placeholder.name_ru" var="placholder_name_ru"/>
                <label for="nameRu"><fmt:message key="theme_form_jsp.label.name_ru"/></label>
                <input type="text" class="form-control" id="nameRu" placeholder="${placholder_name_ru}" name="nameRu" minlength="4">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary"><fmt:message key="theme_form_jsp.button.create"/></button>
            </div>
        </form>
    </div>

</div>




<%--<form action="home" method="post">--%>
<%--    <input type="hidden" name="command" value="addTheme"/>--%>

<%--    <label for="nameEn">Name:</label>--%>
<%--    <input placeholder="Enter english theme name" type="text" id="nameEn" name="nameEn" minlength="4"><br><br>--%>
<%--    <input placeholder="Enter ukrainian theme name" type="text" id="nameUa" name="nameUa" minlength="4"><br><br>--%>
<%--    <input placeholder="Enter russian theme name" type="text" id="nameRu" name="nameRu" minlength="4"><br><br>--%>

<%--    <input type="submit" value="Create" />--%>

<%--</form>--%>


</body>
</html>
