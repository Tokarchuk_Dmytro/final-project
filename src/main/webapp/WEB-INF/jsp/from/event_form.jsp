<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<html>
<c:set var="title" value="Login"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<c:set var="isEdit" value="${event != null}"/>

<div class="container shadow-lg p-3 mb-5 bg-white rounded" style="margin-top:100px;width: 600px">
    <div class="mx-auto" style="width:500px">
        <h2><fmt:message key="event_from_jsp.h.event_form"/> </h2>
        <form action="home" method="post">
            <c:if test="${requestScope.option == 'new'}">
                <input type="hidden" name="command" value="addEvent"/>
            </c:if>
            <c:if test="${requestScope.option == 'edit'}">
                <input type="hidden" name="command" value="editEvent"/>
                <input type="hidden" name="eventId" value="${param.eventId}">
            </c:if>

            <div class="form-group">
                <fmt:message key="event_from_jsp.placeholder.name" var="placeholder_name"/>
                <label for="name"><fmt:message key="event_from_jsp.label.event_name"/></label>
                <input type="text" class="form-control" id="name" placeholder="${placeholder_name}" name="name"
                        <c:if test="${isEdit}"> value="${event.name}" </c:if>/>
            </div>
            <div class="form-group">
                <label for="date"><fmt:message key="event_from_jsp.label.date"/></label>
                <input type="date" class="form-control" id="date" placeholder="Date" name="date"
                        <c:if test="${isEdit}"> value="${event.date}" </c:if>/>
            </div>
            <div class="form-group">
                <fmt:message key="event_from_jsp.placeholder.location" var="placeholder_location"/>
                <label for="location"><fmt:message key="event_from_jsp.label.location"/></label>
                <input type="text" class="form-control" id="location" placeholder="${placeholder_location}" name="location"
                        <c:if test="${isEdit}"> value="${event.location}" </c:if>/>
            </div>
            <div class="form-group">
                <label for="time"><fmt:message key="event_from_jsp.label.time"/></label>
                <input type="time" class="form-control" id="time"  name="time"
                        <c:if test="${isEdit}"> value="${event.time}" </c:if>/>
            </div>
            <div class="form-group">
                <label for="description"><fmt:message key="event_from_jsp.label.description"/></label>
                <textarea class="form-control" rows="5" id="description" name="description" style="max-height: 200px;min-height: 50px"></textarea>
            </div>
            <div class="form-group">
                <fmt:message key="event_from_jsp.placeholder.time_zone" var="placeholder_time_zone"/>
                <label for="timeZone"><fmt:message key="event_from_jsp.label.time_zone"/></label>
                <select class="form-control" id="timeZone" name="timeZone">
                    <option value=""><fmt:message key="event_form_jsp.option.none"/> </option>
                    <option value="UTC-12:00">UTC-12:00</option>
                    <option value="UTC-11:00">UTC-11:00</option>
                    <option value="UTC-10:00">UTC-10:00</option>
                    <option value="UTC-09:30">UTC-09:30</option>
                    <option value="UTC-09:00">UTC-09:00</option>
                    <option value="UTC-07:00">UTC-07:00</option>
                    <option value="UTC-06:00">UTC-06:00</option>
                    <option value="UTC-05:00">UTC-05:00</option>
                    <option value="UTC-04:30">UTC-04:30</option>
                    <option value="UTC-04:00">UTC-04:00</option>
                    <option value="UTC-03:00">UTC-03:00</option>
                    <option value="UTC-02:00">UTC-02:00</option>
                    <option value="UTC-01:00">UTC-01:00</option>
                    <option value="UTC±00:00">UTC±00:00</option>
                    <option value="UTC+01:00">UTC+01:00</option>
                    <option value="UTC+02:00">UTC+02:00</option>
                    <option value="UTC+03:00">UTC+03:00</option>
                    <option value="UTC+04:00">UTC+04:00</option>
                    <option value="UTC+04:30">UTC+04:30</option>
                    <option value="UTC+05:00">UTC+05:00</option>
                    <option value="UTC+05:45">UTC+05:45</option>
                    <option value="UTC+06:00">UTC+06:00</option>
                    <option value="UTC+07:00">UTC+07:00</option>
                    <option value="UTC+08:00">UTC+08:00</option>
                    <option value="UTC+08:45">UTC+08:45</option>
                    <option value="UTC+09:00">UTC+09:00</option>
                    <option value="UTC+09:30">UTC+09:30</option>
                    <option value="UTC+10:00">UTC+10:00</option>
                    <option value="UTC+11:00">UTC+11:00</option>
                    <option value="UTC+11:30">UTC+11:30</option>
                    <option value="UTC+12:00">UTC+12:00</option>
                    <option value="UTC+13:00">UTC+13:00</option>
                    <option value="UTC+14:00">UTC+14:00</option>
                </select>
            </div>
            <div class="form-group">
                <label for="language"><fmt:message key="event_from_jsp.label.select_event_language"/></label>
                <select class="form-control" id="language" name="languageId">
                    <option value="0"><fmt:message key="language_en"/></option>
                    <option value="1"><fmt:message key="language_ua"/></option>
                    <option value="2"><fmt:message key="language_ru"/></option>
                </select>
            </div>
            <div class="text-center">
                    <c:if test="${requestScope.option == 'new'}">
                        <button type="submit" class="btn btn-primary"><fmt:message key="event_from_jsp.button.create"/></button>
                    </c:if>


                    <c:if test="${requestScope.option == 'edit'}">
                        <button type="submit" class="btn btn-primary"><fmt:message key="event_from_jsp.button.edit"/></button>
                    </c:if>
            </div>
        </form>
    </div>

</div>
</body>
</html>
