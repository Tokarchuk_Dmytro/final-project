<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/contains.tld" prefix="custom" %>

<html>
<fmt:message key="home_jsp.title" var="title"/>
<%--<c:set var="title" value="Conferences home"/>--%>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>


<div class="container-fluid" style="margin-top:90px">


    <div class="row rounded-top mx-2">
        <div class="col-sm-2">
        </div>

        <div class="col-sm-3 mx-3 border rounded-top" style="background-color: rgba(0,129,177,0.1)">
            <form>
                <input type="hidden" name="command" value="home">
                <h6><fmt:message key="home_jsp.h6.event_relevance"/></h6>
                <div class="row">
                    <div class="col-6">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="eventRelevance"
                                       checked="checked" ${sessionScope.eventRelevance=='future' ? 'checked="checked"' : ''}
                                       value="future"><fmt:message key="home_jsp.radio_button.future"/>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="eventRelevance"
                                ${sessionScope.eventRelevance=='past' ? 'checked="checked"' : ''}
                                       value="past"><fmt:message key="home_jsp.radio_button.past"/>
                            </label>
                        </div>
                    </div>
                    <div class="col-6 mt-4">
                        <button type="submit" class="btn btn-outline-primary btn-sm"><fmt:message
                                key="home_jsp.button.apply"/></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-3 mx-3 border rounded-top" style="background-color: rgba(0,129,177,0.1)">
            <form>
                <input type="hidden" name="command" value="home">
                <h6><fmt:message key="home_jsp.h6.event_language"/></h6>
                <div class="row">
                    <div class="col-6">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value="0" name="languages"
                                <c:if test="${ custom:contains(eventLanguages,0) }"> checked</c:if>><fmt:message
                                    key="home_jsp.checkbox.english"/>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value="1" name="languages"
                                <c:if test="${ custom:contains(eventLanguages,1) }"> checked</c:if>><fmt:message
                                    key="home_jsp.checkbox.ukrainian"/>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value="2" name="languages"
                                <c:if test="${ custom:contains(eventLanguages,2) }"> checked</c:if>><fmt:message
                                    key="home_jsp.checkbox.russian"/>
                            </label>
                        </div>
                    </div>
                    <div class="col-6 mt-4 ">
                        <button type="submit" class="btn btn-outline-primary btn-sm"><fmt:message
                                key="home_jsp.button.apply"/></button>
                    </div>
            </form>
        </div>

    </div>
    </span>

    <div class="col-sm-3 mx-3 border rounded-top" style="background-color: rgba(0,129,177,0.1)">
        <h6><fmt:message key="home_jsp.h6.sort_order"/></h6>
        <form>
            <input type="hidden" name="command" value="home">
            <div class="row">
                <div class="col-6 pt-3">
                    <select class="form-control" id="sel1" name="sortOrder">
                        <option value="asc"  ${sessionScope.sortOrder=='asc' ? 'selected' : ''}  ><fmt:message
                                key="home_jsp.select.option.increasing"/></option>
                        <option value="desc" ${sessionScope.sortOrder=='desc' ? 'selected' : ''} ><fmt:message
                                key="home_jsp.select.option.decreasing"/></option>
                    </select>
                </div>
                <div class="col-6 mt-4 ">
                    <button type="submit" class="btn btn-outline-primary btn-sm"><fmt:message
                            key="home_jsp.button.apply"/></button>
                </div>
            </div>
        </form>
    </div>

</div>


<div>
    <table class="table table-bordered">
        <thead class="table-primary">
        <tr>
            <th class="tg-c3ow"><fmt:message key="home_jsp.table.header.name"/></th>
            <th class="tg-c3ow"><a href="/home?command=home&sortOption=date"><fmt:message
                    key="home_jsp.table.header.date"/></a></th>
            <th class="tg-c3ow"><fmt:message key="home_jsp.table.header.time"/></th>
            <th class="tg-c3ow"><fmt:message key="home_jsp.table.header.location"/></th>
            <th class="tg-c3ow"><fmt:message key="home_jsp.table.header.time_zone"/></th>
            <th class="tg-c3ow"><a href="/home?command=home&sortOption=users"><fmt:message
                    key="home_jsp.table.header.users"/></a></th>
            <th class="tg-c3ow"><a href="/home?command=home&sortOption=reports"><fmt:message
                    key="home_jsp.table.header.reports"/></a></th>
            <c:if test="${userRole.name eq 'moderator'}">
                <th class="tg-c3ow"><fmt:message key="home_jsp.table.header.users_presence"/></th>
            </c:if>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="event" items="${events}">
            <tr>
                <td class="tg-0pky"><a class="aStyle"
                                       href="/home?command=showEvent&id=${event.id}"> ${event.name} </a>
                </td>
                <td class="tg-c3ow">${event.date}</td>
                <td class="tg-c3ow">${event.time}</td>
                <td class="tg-c3ow">${event.location}</td>
                <td class="tg-c3ow">${event.timeZone}</td>
                <td class="tg-c3ow">${event.subscribers}</td>
                <td class="tg-c3ow">${event.reports}</td>
                <c:if test="${userRole.name eq 'moderator'}">
                    <td class="tg-c3ow">${event.usersPresence}</td>
                </c:if>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<%--<span class="navigationSpan">--%>
<%--    Navigation:     (Pages: ${pages})--%>
<%--    <c:if test="${pageBegin != 1}">--%>
<%--        ...--%>
<%--    </c:if>--%>

<%--        <c:forEach var="pageNum" begin="${pageBegin}" end="${pageEnd}">--%>

<%--            <c:choose>--%>
<%--                <c:when test="${pageNum-1 == currentPage}">--%>
<%--                <a style="color: red; font-size: 20px; text-decoration: none"--%>
<%--                   href="/home?command=home&page=${pageNum-1}">--%>
<%--                    <c:out value="${pageNum}"/>--%>
<%--                </a>--%>
<%--                </c:when>--%>
<%--                <c:otherwise>--%>
<%--                    <a class="aStyle" href="/home?command=home&page=${pageNum-1}">--%>
<%--                    <c:out value="${pageNum}"/>--%>
<%--                </a>--%>
<%--                </c:otherwise>--%>
<%--            </c:choose>--%>

<%--            <c:if test="${pageNum != pageEnd}">--%>
<%--                ---%>
<%--            </c:if>--%>

<%--        </c:forEach>--%>

<%--    <c:if test="${pageEnd != pages}">--%>
<%--        ...--%>
<%--    </c:if>--%>
<%--</span>--%>
<div class="container-fluid">

    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="/home?command=home&page=0"><fmt:message key="home_jsp.pagination.first"/> </a></li>
        <c:if test="${pageBegin != 1}">
            <li class="page-item disabled"><a class="page-link">...</a></li>
        </c:if>

        <c:forEach var="pageNum" begin="${pageBegin}" end="${pageEnd}">

            <c:choose>
                <c:when test="${pageNum-1 == currentPage}">
                    <li class="page-item active"><a class="page-link"
                                                    href="/home?command=home&page=${pageNum-1}">${pageNum}</a></li>
                </c:when>
                <c:otherwise>
                    <li class="page-item"><a class="page-link"
                                             href="/home?command=home&page=${pageNum-1}">${pageNum}</a></li>
                </c:otherwise>
            </c:choose>

        </c:forEach>

        <c:if test="${pageEnd != pages}">
            <li class="page-item disabled"><a class="page-link">...</a></li>
        </c:if>
        <li class="page-item"><a class="page-link" href="/home?command=home&page=${pageEnd-1}"><fmt:message key="home_jsp.pagination.last"/></a></li>
    </ul>

</div>

</body>
</html>
