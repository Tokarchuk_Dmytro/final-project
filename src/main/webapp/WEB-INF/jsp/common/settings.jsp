<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="Login"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>


<div class="container shadow-lg p-3 mb-5 bg-white rounded" style="margin-top:160px;width: 600px">
    <div class="mx-auto" style="width:500px">
        <h2><fmt:message key="setting_jsp.h.settings"/></h2>
        <form action="home" method="post">
            <input type="hidden" name="command" value="changeSettings"/>
            <div class="form-group">
                <label for="sel1"><fmt:message key="setting_jsp.label.select_preferred_language"/></label>
                <select class="form-control" id="sel1" name="languageId">
                    <option value="${userLanguage.id}">${userLanguage.name}</option>
                    <c:forEach items="${languages}" var="language">
                        <option value="${language.id}">${language.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="text-center">
                <fmt:message key="setting_jsp.button.confirm" var="buttom_confirm"/>
                <button type="submit" class="btn btn-primary">${buttom_confirm}</button>
            </div>
        </form>
    </div>

</div>


</body>
</html>
