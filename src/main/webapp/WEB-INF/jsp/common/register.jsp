<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="Login"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>



<div class="container shadow-lg p-3 mb-5 bg-white rounded" style="margin-top:100px;width: 600px">
    <div class="mx-auto" style="width:500px">
        <h2><fmt:message key="register_jsp.h.registration"/></h2>
        <form action="home" method="post">
            <input type="hidden" name="command" value="register"/>
            <div class="form-group">
                <fmt:message key="register_jsp.placeholder.full_name" var="placeholder_name"/>
                <label for="fullName"><fmt:message key="register_jsp.label.full_name"/></label>
                <input type="text" class="form-control" id="fullName" placeholder="${placeholder_name}" name="fname">
            </div>
            <div class="form-group">
                <fmt:message key="register_jsp.placeholder.email" var="placeholder_email"/>
                <label for="email"><fmt:message key="register_jsp.label.email"/></label>
                <input type="email" class="form-control" id="email" placeholder="${placeholder_email}" name="email">
            </div>
            <div class="form-group">
                <fmt:message key="register_jsp.placeholder.password" var="placeholder_password"/>
                <label for="pwd"><fmt:message key="register_jsp.label.password"/></label>
                <input type="password" class="form-control" id="pwd" placeholder="${placeholder_password}" name="password">
            </div>
            <div class="form-group">
                <fmt:message key="register_jsp.placeholder.password_confirm" var="placeholder_password_cong"/>
                <label for="pwdConf"><fmt:message key="register_jsp.label.password_conf"/></label>
                <input type="password" class="form-control" id="pwdConf" placeholder="${placeholder_password_cong}" name="password_conf">
            </div>
            <div class="form-group">
                <label for="sel1"><fmt:message key="register_jsp.label.language"/></label>
                <select class="form-control" id="sel1" name="language">
                    <option value="0"><fmt:message key="language_en"/></option>
                    <option value="1"><fmt:message key="language_ua"/></option>
                    <option value="2"><fmt:message key="language_ru"/></option>
                </select>
            </div>
            <div class="text-center">
                <div class="container">
                    <p class="text-left" style="font-size: 12px">
                        <fmt:message key="register_jsp.requirements"/>
                    </p>
                </div>
            <button type="submit" class="btn btn-primary"><fmt:message key="register_jsp.button.register"/></button>
            </div>
        </form>
    </div>

</div>



</body>
</html>
