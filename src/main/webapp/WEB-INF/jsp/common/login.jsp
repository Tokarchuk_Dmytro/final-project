<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<%--<c:set var="title" value="Login"/>--%>
<fmt:message key="login_jps.title" var="title"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>


<div class="container shadow-lg p-3 mb-5 bg-white rounded" style="margin-top:160px;width: 600px">
    <div class="mx-auto" style="width:500px">
    <h2><fmt:message key="login_jps.h.login"/></h2>
    <form action="home" method="post">
        <input type="hidden" name="command" value="login"/>
        <div class="form-group">
            <fmt:message key="login_jps.placeholder.email" var="placeholder_email"/>
            <label for="email"><fmt:message key="login_jps.label.email"/></label>
            <input type="email" class="form-control" id="email" placeholder="${placeholder_email}" name="email">
        </div>
        <div class="form-group">
            <fmt:message key="login_jps.placeholder.password" var="placeholder_password"/>
            <label for="pwd"><fmt:message key="login_jps.label.password"/></label>
            <input type="password" class="form-control" id="pwd" placeholder="${placeholder_password}" name="password">
        </div>
        <div class="text-center">
        <button type="submit" class="btn btn-primary"><fmt:message key="login_jps.button.submit"/></button>
        </div>
    </form>
    </div>

</div>

</body>
</html>
