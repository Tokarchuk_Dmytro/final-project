<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<html>
<c:set var="title" value="${event.name}"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<jsp:useBean id="today" class="java.util.Date"/>
<c:set var="eventIsPassed" value="${exactEventTime lt today.time}"/>
<c:set var="speaker" value="speaker"/>
<c:set var="modearator" value="moderator"/>

<div class="container-fluid bg-primary text-white" style="margin-top:80px">
    <h2 class="display-3">${event.name}</h2>

    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1 class="display-5"><fmt:message key="event_jsp.h.where"/></h1>
            <h3>${event.location}</h3>
        </div>
        <div class="col-sm-4">
            <h1 class="display-5"><fmt:message key="event_jsp.h.when"/></h1>
            <h3>${event.date}, ${event.time} ${event.timeZone}</h3>
        </div>
    </div>
    <div class="row">

        <div class="container-fluid bg-light text-dark border">
            <div class="mx-auto " style="width:900px">
                <h3><fmt:message key="event_jsp.h.description"/></h3>
                <p style="margin-left: 20px">${event.description}</p>

            </div>
            <div class="row">
                <div class="col-sm-2">

                </div>
                <div class="col-sm-2">

                    <c:if test="${!eventIsPassed}">
                        <c:if test="${userRole.name == 'moderator'}">
                            <form action="/home" method="get">
                                <input type="hidden" name="command" value="viewEventForm"/>
                                <input type="hidden" name="eventId" value="${event.id}"/>
                                <input type="hidden" name="option" value="edit">
                                <button type="submit" class="btn btn-primary btn-sm"><fmt:message key="event_jsp.button.edit_event"/></button>
                            </form>
                        </c:if>
                    </c:if>
                </div>
                <div class="col-sm-2">
                    <c:if test="${!eventIsPassed}">
                        <c:if test="${userRole.name == 'moderator'}">
                            <form action="/home" method="get">
                                <input type="hidden" name="command" value="viewReportForm"/>
                                <input type="hidden" name="eventId" value="${event.id}"/>
                                <input type="hidden" name="option" value="add">
                                <button type="submit" class="btn btn-primary btn-sm"><fmt:message key="event_jsp.button.add_report"/></button>
                            </form>
                        </c:if>
                    </c:if>
                </div>
                <div class="col-sm-2">
                    <div class="mx-auto" style="width: 200px">
                        <c:choose>
                            <c:when test="${user == null}">
                                <p class="text-info"><fmt:message key="event_jsp.p.login_info"/></p>
                            </c:when>
                            <c:when test="${eventIsPassed}">
                                <p class="text-info"><fmt:message key="event_jsp.p.event_passed"/></p>
                            </c:when>
                            <c:when test="${userRole.name == 'user'}">
                                <form action="/home" method="get">
                                    <input type="hidden" name="command" value="singUserForEvent"/>
                                    <input type="hidden" name="id" value="${event.id}"/>
                                    <button type="submit" class="btn btn-primary btn-sm"><fmt:message key="event_jsp.button.register_for_event"/></button>
                                </form>
                                <br/>
                            </c:when>
                        </c:choose>
                    </div>
                </div>
                <div class="col-sm-2">
                        <c:if test="${userRole.name == 'speaker' && !eventIsPassed}">
                            <form action="/home" method="get">
                                <input type="hidden" name="command" value="viewReportForm"/>
                                <input type="hidden" name="eventId" value="${event.id}"/>
                                <input type="hidden" name="option" value="suggest">
                                <input type="hidden" name="by" value="user">
                                <button type="submit" class="btn btn-primary btn-sm"><fmt:message key="event_jsp.button.suggest_report"/></button>
                            </form>
                        </c:if>
                </div>
                <div class="col-sm-2">

                </div>


            </div>
            <div class="mx-auto" style="width:900px">
                <h3><fmt:message key="event_jsp.h.featuring_reports"/></h3>

                <table class="table table-bordered">
                    <thead class="table-primary">
                    <tr>
                        <th class="tg-c3ow"><fmt:message key="event_jsp.table.name"/></th>
                        <th class="tg-c3ow"><fmt:message key="event_jsp.table.theme"/></th>
                        <th class="tg-c3ow"><fmt:message key="event_jsp.table.speaker"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="report" items="${reportBeans}">
                        <tr>
                            <td class="tg-0pky">${report.reportName}</td>
                            <td class="tg-c3ow">${report.themeName} </td>
                            <td class="tg-c3ow">

                                <c:choose>
                                    <c:when test="${report.userName eq null}">
                                        <c:choose>
                                            <c:when test="${userRole.name == speaker}">
                                                <c:if test="${report.statusId eq  0  && !eventIsPassed}">
                                                    <a href="/home?command=submitForSpeaker&reportId=${report.reportId}&eventId=${event.id}"><fmt:message key="event_jsp.a.submit_as_speaker"/></a>
                                                </c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <p class="text-warning"><fmt:message key="event_jsp.p.no_speaker_yet"/></p>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${userRole.name eq speaker}">
                                                <c:choose>
                                                    <c:when test="${report.statusId eq 3 && report.userId eq sessionScope.user.id}">
                                                        <p class="text-success"><fmt:message key="event_jsp.p.you_submitted_as_speaker"/></p>
                                                    </c:when>
                                                    <c:when test="${report.statusId eq 1 && report.userId eq sessionScope.user.id}">
                                                        <p class="text-primary font-italic"><fmt:message key="event_jsp.p.invited_as_speaker"/></p>
                                                    </c:when>
                                                    <c:when test="${report.userId eq sessionScope.user.id}">
                                                        <p class="text-success font-italic">${report.userName} <fmt:message key="event_jsp.p.you"/></p>

                                                    </c:when>
                                                    <c:when test="${report.statusId eq 0 && report.userId ne null}">
                                                        <c:out value="${report.userName}"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <p class="text-info font-italic"><fmt:message key="event_jsp.p.already_submitted"/></p>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:when test="${userRole.name eq modearator}">
                                                <c:choose>
                                                    <c:when test="${report.statusId eq 1}">
                                                        <p class="text-info font-italic"><fmt:message key="event_jsp.p.waiting_for"/> ${report.userName} <fmt:message key="event_jsp.p.response"/></p>
                                                    </c:when>
                                                    <c:when test="${report.statusId eq 3}">
                                                        <p class="text-info font-italic">${report.userName} <fmt:message key="event_jsp.p.submitted_as_speaker"/></p>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:out value="${report.userName}"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                <c:choose>
                                                    <c:when test="${report.statusId eq 0}">
                                                        <c:out value="${report.userName}"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <p class="text-muted"><fmt:message key="event_jsp.p.no_speaker"/></p>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <c:if test="${userRole.name == 'moderator'  && !eventIsPassed}">
                                <td>
                                    <a href="/home?command=viewReportForm&option=edit&reportId=${report.reportId}&eventId=${event.id}">
                                        <fmt:message key="event_jsp.a.edit"/> </a>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>


            </div>
        </div>
    </div>

</div>


<%--This is a: ${event.name}<br>--%>
<%--About:--%>
<%--<c:if test="${event.description != null}">--%>
<%--    ${event.description}<br>--%>
<%--</c:if>--%>

<%--<br>--%>
<%--Where: ${event.location}<br>--%>
<%--When: ${event.date} ${event.time}<br>--%>
<%--<c:if test="${event.timeZone != null}">--%>
<%--    Time zone: ${event.timeZone}<br>--%>
<%--</c:if>--%>
<%--<c:if test="${!eventIsPassed}">--%>
<%--    <c:if test="${userRole.name == 'moderator'}">--%>
<%--        <form action="/home" method="get">--%>
<%--            <input type="hidden" name="command" value="viewEventForm"/>--%>
<%--            <input type="hidden" name="eventId" value="${event.id}"/>--%>
<%--            <input type="hidden" name="option" value="edit">--%>
<%--            <input type="submit" value="Edit event"/>--%>
<%--        </form>--%>

<%--        <form action="/home" method="get">--%>
<%--            <input type="hidden" name="command" value="viewReportForm"/>--%>
<%--            <input type="hidden" name="eventId" value="${event.id}"/>--%>
<%--            <input type="hidden" name="option" value="add">--%>
<%--            <input type="submit" value="Add report"/>--%>
<%--        </form>--%>
<%--    </c:if>--%>
<%--    <c:if test="${userRole.name == 'speaker'}">--%>
<%--        <form action="/home" method="get">--%>
<%--            <input type="hidden" name="command" value="viewReportForm"/>--%>
<%--            <input type="hidden" name="eventId" value="${event.id}"/>--%>
<%--            <input type="hidden" name="option" value="suggest">--%>
<%--            <input type="hidden" name="by" value="user">--%>
<%--            <input type="submit" value="Suggest report"/>--%>
<%--        </form>--%>
<%--    </c:if>--%>
<%--</c:if>--%>
<%--Featuring reports:--%>
<%--<br><br/>--%>


<%--<table class="tg" style="table-layout: auto; width: 646px">--%>
<%--    <colgroup>--%>
<%--        <col style="width: 245px">--%>
<%--        <col style="width: 99px">--%>
<%--        <col style="width: 60px">--%>
<%--        <col style="width: 174px">--%>
<%--        <col style="width: 68px">--%>
<%--    </colgroup>--%>
<%--    <thead>--%>
<%--    <tr>--%>
<%--        <th class="tg-c3ow">Name</th>   --%>
<%--        <th class="tg-c3ow">Theme</th>  --%>
<%--        <th class="tg-c3ow">Speaker</th>--%>
<%--    </tr>--%>
<%--    </thead>--%>
<%--    <tbody>--%>
<%--    <c:forEach var="report" items="${reportBeans}">--%>
<%--        <tr>--%>
<%--            <td class="tg-c3ow">${report.reportName}</td>--%>
<%--            <td class="tg-c3ow">${report.themeName} </td>--%>
<%--            <td class="tg-c3ow">--%>
<%--                <c:choose>--%>

<%--                    <c:when test="${report.userName eq null}">--%>
<%--                        <c:choose>--%>

<%--                            <c:when test="${userRole.name == speaker}">--%>
<%--                                <c:if test="${report.statusId eq 0  && !eventIsPassed}">--%>
<%--                                    <a href="/home?command=submitForSpeaker&reportId=${report.reportId}&eventId=${event.id}">Submit--%>
<%--                                        as speaker</a>--%>
<%--                                </c:if>--%>
<%--                            </c:when>--%>
<%--                            <c:otherwise>--%>
<%--                                <c:out value="No speaker yet"/>--%>
<%--                            </c:otherwise>--%>

<%--                        </c:choose>--%>

<%--                    </c:when>--%>
<%--                    <c:otherwise>--%>
<%--                        <c:choose>--%>
<%--                            <c:when test="${userRole.name eq speaker}">--%>
<%--                                <c:choose>--%>
<%--                                    <c:when test="${report.statusId eq 3 && report.userName eq sessionScope.user.name}">--%>
<%--                                        You submitted as speaker--%>
<%--                                    </c:when>--%>
<%--                                    <c:otherwise>--%>
<%--                                        <c:out value="Someone already submited"/>--%>
<%--                                    </c:otherwise>--%>
<%--                                </c:choose>--%>
<%--                            </c:when>--%>
<%--                            <c:when test="${userRole.name eq modearator}">--%>
<%--                                <c:choose>--%>
<%--                                    <c:when test="${report.statusId eq 1}">--%>
<%--                                        ${report.userName} waiting for response--%>
<%--                                    </c:when>--%>
<%--                                    <c:when test="${report.statusId eq 3}">--%>
<%--                                        ${report.userName} waiting for response--%>
<%--                                    </c:when>--%>
<%--                                </c:choose>--%>
<%--                            </c:when>--%>
<%--                            <c:otherwise>--%>
<%--                                <c:out value="No speaker"/>--%>
<%--                            </c:otherwise>--%>
<%--                        </c:choose>--%>
<%--                    </c:otherwise>--%>

<%--                </c:choose>--%>
<%--            </td>--%>
<%--            <c:if test="${userRole.name == 'moderator'  && !eventIsPassed}">--%>
<%--                <td>--%>
<%--                    <a href="/home?command=viewReportForm&option=edit&reportId=${report.reportId}&eventId=${event.id}">--%>
<%--                        edit </a>--%>
<%--                </td>--%>
<%--            </c:if>--%>
<%--        </tr>--%>
<%--    </c:forEach>--%>
<%--    </tbody>--%>
<%--</table>--%>

<%--<br/>--%>
<%--<c:choose>--%>
<%--    <c:when test="${user == null}">--%>
<%--        Login or create a new account to follow this event--%>
<%--        <br/>--%>
<%--    </c:when>--%>
<%--    <c:when test="${eventIsPassed}">--%>
<%--        <input type="submit" value="Sing up for event" disabled>--%>
<%--    </c:when>--%>
<%--    <c:when test="${userRole.name == 'user'}">--%>
<%--        <form action="/home" method="get">--%>
<%--            <input type="hidden" name="command" value="singUserForEvent"/>--%>
<%--            <input type="hidden" name="id" value="${event.id}"/>--%>
<%--            <input type="submit" value="Sing up for event"/>--%>
<%--        </form>--%>
<%--        <br/>--%>
<%--    </c:when>--%>

<%--</c:choose>--%>


</body>
</html>
