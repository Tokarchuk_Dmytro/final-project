<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<html>
<c:set var="title" value="Registration"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>


    <h1>Conferences</h1>

            <h2>Registration Form</h2>


            <form id="registration_form" action="home" method="post">
                <input type="hidden" name="command" value="register"/>

                <label for="fname">Full name:</label>
                <input type="text" id="fname" name="fname"><br><br>

                <label for="email">Email:</label>
                <input type="email" id="email" name="email"><br><br>

                <label for="password">Password:</label>
                <input type="password" id="password" name="password"><br><br>

                <label for="password_conf">Confirm password:</label>
                <input type="password" id="password_conf" name="password_conf"><br><br>

                <label for="language">Choose a language:</label><br><br>
                <select id="language" name="language">

                    <c:forEach var="language" items="${languages}" varStatus="loop">
                            <option value="${loop.index}">${language.name}</option>
                    </c:forEach>
                </select>
                <br><br>

        <input type="submit" value="Register" />

</form>





</body>
</html>