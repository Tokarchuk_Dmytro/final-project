<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<c:set var="title" value="Login" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>


<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="loginDiv">

    <title>Login</title>

    <h2>Login</h2>


    <form id="login_form" action="home" method="post">
        <input type="hidden" name="command" value="login"/>

        <label for="email">Email:</label>
        <input placeholder="Enter email" type="email" id="email" name="email"><br><br>

        <label for="password">Password:</label>

        <input placeholder="Enter password" type="password" id="password" name="password">

        <span><input type="submit" value="Login"/></span>

    </form>

    <c:forEach var="cars" items="">
        ${cars.name}  ${cars.car}
        <fmt:message var="color.${cars.colorName}"/>

    </c:forEach>


</div>
</body>
</html>